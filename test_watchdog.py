import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler

class TrackingPlantLib(FileSystemEventHandler):
    def setup(self, path='', callback=None):
        self.plantlib_path = path
        self.callback = callback

    def on_modified(self, event):
        print("plant lib modified: {}".format(event))
        try:
            if event.src_path == self.plantlib_path:
                self.callback(event)
        except:
            pass

    def on_created(self, event):
        self.on_modified(event)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else './assets/'
    event_handler = TrackingPlantLib()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()