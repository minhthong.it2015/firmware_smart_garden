# coding=utf-8

from modules.configuring.configs_mgr import ConfigManager
from modules.tracker.environment_log import EnvironmentLog
from modules.tracker.devices_log import DevicesLog

from modules.equipment.esp_device import ESPDevice

import asyncio
import random
import datetime

class Station:
  def __init__(self, info, user_plant_mgr=None):
    self.id = info['id']
    self.name = info['name']
    self.type = info['type']
    self.env_type = info['env_type'] if 'env_type' in info else ['all']
    self.equipments_info = info['equipments'] if 'equipments' in info else []
    self.equipments = [] # equipment list được nạp sau từ equipment_mgr
    self.user_plant_mgr = user_plant_mgr
    self.automation_mode = True
    self.env_log = EnvironmentLog()
    self.devices_log = DevicesLog()

  def attach_equipment(self, equip):
    self.equipments.append(equip)
    equip.bound_to_station(self)
    equip.addEventListener("statechange", lambda event: self.update_state(event["state"]))
  
  def update(self, new_station):
    self.name = new_station.name
    self.type = new_station.type
    self.env_type = new_station.env_type
    self.equipments_info = new_station.equipments_info
    # self.equipments = [] # được nạp sau từ equipment_mgr

  def ensure_living_environment(self):
    if not self.automation_mode: return
    # print("[Station {}] > Start ensure living environment.".format(self.id), flush=True)
    problems = []
    for user_plant in self.plants:
      problems += user_plant.check_living_environment(self.state, self.env_type)
    if len(problems) > 0:
      # print("[Station {}] > ------------- Actions -------------".format(self.id))
      self.resolve_problems(problems)
      return self.id, False
    return self.id, True
  
  def resolve_problems(self, problems):
    """ Hàm quy định cách xử lý đối với các vấn đề. """
    actions = {}
    for i, problem in enumerate(problems):
      # print("--> {}: ".format(i), problem['problem'])
      for action in problem['actions']:
        if action['action'] not in actions:
          actions[action['action']] = []
        if action['reasons'] not in actions[action['action']]:
          if isinstance(action['reasons'], list):
            actions[action['action']] += action['reasons']
          else:
            actions[action['action']].append(action['reasons'])
    self.do_actions(actions)
  
  def do_actions(self, actions):
    for action in actions:
      parts = action.split("-")
      if len(parts) == 1:
        device_role = parts[0]
        state = 'on'
        duration = None
      elif len(parts) == 2:
        device_role, state = parts
        duration = None
      elif len(parts) == 3:
        device_role, state, duration = parts
      state = state == 'on'
      reasons = list(map(lambda reason: reason + '_' + self.id, actions[action]))
      if duration != None: # Định giờ nâng cao
        pass
      self.switch(device_role, state, reasons)

  def switch(self, device_role, state, reason=''):
    """ Thực hiện bật tắt thiết bị """
    if device_role == 'automation_mode':
      if state == True: # Chuyển sang tự động
        self.automation_mode = True
        # for equipment in self.equipments:
        #   equipment.off(reason)
      else: # Chuyển sang thủ công
        self.automation_mode = False
    else:
      if reason == 'user_set' and self.automation_mode:
        print("[Station {}] > Turn off Automation_Mode first".format(self.id))
        return
      for equipment in self.equipments:
        if device_role in equipment.roles and equipment.state[device_role] != state:
          equipment.put_state(device_role, state)
          equipment.emit_state_change_event()
          # print("[Station {}] ~> set {} to {} ({})".format(self.id, equipment.id, state, reason))

  @property
  def plants(self):
    return self.user_plant_mgr.get_plants_by_location(self.id) if self.user_plant_mgr else []

  @property
  def plants_dump(self):
    return self.user_plant_mgr.dump_plants_by_location(self.id) if self.user_plant_mgr else []

  def dump(self):
    station_equipments = []
    for equipment in self.equipments:
      station_equipments.append(equipment.dump())

    station = {
      "name": self.name,
      "id": self.id,
      "plants": self.plants_dump,
      "equipments": station_equipments,
      "type": self.type,
      "automation_mode": self.automation_mode,
      "state": self.state,
      "evaluations": self.evaluations
    }

    return station

  @property
  def state(self):
    state = {}
    for equip in self.equipments: # equip is virtal_device
      for real_device in equip._real_devices:
        if real_device.state and isinstance(real_device.state, dict):
          state.update(real_device.state)
    return state
  
  @state.setter
  def state(self, state):
    self.ensure_living_environment()
    self.env_log.log(self.id, '', state)

  def update_state(self, state):
    self.state = state

  def set_state(self, equipID, state, reason=''):
    for equip in self.equipments: # equip is virtal_device
      if equip.id == equipID:
        equip.set_state(state)

  @property
  def evaluations(self):
    evaluations = {}
    for factor in self.state:
      # for plant in self.plants:
      #   # Kiểm tra thông qua PlantLib
      evaluations.update({
        factor: self.evaluate(factor, self.state[factor])
      })
    return evaluations
  
  @staticmethod
  def evaluate(factor_name, factor_value):
    best_value = Station.get_best_value(factor_name)
    if factor_name == "temperature":
      return 10 - abs(factor_value - best_value)
    elif factor_name == "light":
      return 10 - abs(factor_value - best_value)/200
    elif factor_name == "humidity":
      return 10 - abs(factor_value - best_value)/10

  
  @staticmethod
  def get_best_value(factor_name):
    h = datetime.datetime.now().hour
    if factor_name == "temperature":
      return ESPDevice.calcTemp(h)
    elif factor_name == "light":
      return ESPDevice.calcLight(h)
    elif factor_name == "humidity":
      return ESPDevice.calcHumi(h)

