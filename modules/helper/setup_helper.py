
import os
cmd = os.system

def generate_service_file(service_name, exec_path, working_directory, description):
  return """# /etc/systemd/system

[Unit]
Description={}
After=network.target multi-user.target

[Service]
Type=idle
ExecStart={}/{}
WorkingDirectory={}
Restart=always
User=pi

[Install]
WantedBy=network.target multi-user.target
Alias={}.service
""".format(description, working_directory, exec_path, working_directory, service_name)

class FirmwareService:
  def __init__(self, name='InSys', exec_file='startup', description='Insys Firmware Service', enable=True):
    self.name = name
    self.exec_file = exec_file
    self.description = description
    self.enable = enable
  
  @property
  def service_name(self):
    return self.name.replace(' ', '').lower()
  
  @property
  def cwd(self):
    return os.getcwd()
  
  @property
  def service_file(self):
    return generate_service_file(self.service_name, self.exec_file, self.cwd, self.description)

  def install(self):
    if not self.enable: return # Bỏ qua các dịch vụ ẩn
    # Xóa file service cũ
    cmd('sudo rm /etc/systemd/system/{}.service'.format(self.service_name))

    cmd('chmod +x {}'.format(self.exec_file))
    print('[Setup] > Install {} service'.format(self.service_name))
    insys_service_file = open('{}.service'.format(self.service_name), 'w')
    insys_service_file.write(self.service_file)
    insys_service_file.close()
    cmd('sudo mv {}.service /etc/systemd/system'.format(self.service_name))
    cmd('chmod +x /etc/systemd/system/{}.service'.format(self.service_name))
    # cmd('sudo systemctl enable {}.service'.format(self.service_name))
    cmd('sudo systemctl daemon-reload')
    cmd('sudo systemctl start {}.service'.format(self.service_name))
    # cmd('sudo systemctl status {}'.format(service_name))
  
  def restart(self):
    if not self.enable: return
    cmd('sudo systemctl restart {}.service'.format(self.service_name))
  
  def stop(self):
    if not self.enable: return
    cmd('sudo systemctl stop {}.service'.format(self.service_name))




##########################################################
###                Helper cho Dependences              ###
##########################################################

def install_dependences(dependences):
  ## Cập nhập python3 nếu cần
  # cmd('sudo apt-get install python3')
  # cmd('sudo apt-get update')
  # cmd('sudo apt-get install build-essential python-dev')

  # Cài module websockets cho kết nối mobile
  try: import websockets
  except: cmd('pip3 install websockets')

  # Cài module ifcfg để lấy ip wlan0
  try: import ifcfg
  except: cmd("pip3 install ifcfg")

  # Cài module để theo dõi thay đổi trên các file asset, config...
  try: import watchdog
  except: cmd("sudo apt-get install python3-watchdog")

  # Cài thư viện cho camera streaming
  # cmd('sudo apt-get install libmp3lame-dev -y; sudo apt-get install autoconf -y; sudo apt-get install libtool -y; sudo apt-get install checkinstall -y; sudo apt-get install libssl-dev -y')
  # cmd('mkdir /home/pi/src && cd /home/pi/src && git clone git://git.videolan.org/x264 && cd x264 && ./configure --host=arm-unknown-linux-gnueabi --enable-static --disable-opencl && sudo make && sudo make install')
  # cmd('cd && cd /home/pi/src && sudo git clone git://source.ffmpeg.org/ffmpeg.git && cd ffmpeg && sudo ./configure --enable-gpl --enable-nonfree --enable-libx264 --enable-libmp3lame && sudo make -j$(nproc) && sudo make install')
  # cmd('sudo apt-get install -y libx264-dev')
  # cmd('sudo apt-get install -y ffmpeg')


def enable_excutable_files(executable_files):
  # Thêm quyền thực thi cho một số file thường dùng
  for file in executable_files: cmd("chmod +x {}".format(file))