
import socket, ifcfg, platform


def isRaspi():
  """ Kiểm tra chạy trên windows hay raspi """
  return 'Windows' not in platform.system()

def ipv4():
  try:
    ip = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2][::-1] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]
  except:
    ip = 'localhost'
  if ip == 'no IP found': ip = ''
  return ip

def ipGarden(gardenSignals):
    ip = ipv4()
    if not ip or ip == 'localhost' or not [ip_signal for ip_signal in gardenSignals if ip_signal in ip]:
      try:
        ip = ''
        interfaces = ifcfg.interfaces()
        wifi_card = 'wlan0' if isRaspi else 'Wireless LAN adapter Wi-Fi'
        if wifi_card not in interfaces: ip = ''
        else: ip = interfaces[wifi_card]['inet4'][::-1][0]
      except:
        ip = ''
    return ip

def ip_garden_or_v4(gardenSignals):
  sIPgarden, sIPv4 = ipGarden(gardenSignals), ipv4()
  return sIPgarden if sIPgarden else sIPv4