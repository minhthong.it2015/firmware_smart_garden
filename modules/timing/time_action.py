# coding=utf-8

from datetime import timedelta
import datetime


class TimeAction:
  """
  Module TimeAction hỗ trợ 3 chế độ hẹn giờ:
  - Thời điểm chính xác hằng ngày/tuần/anything, và kéo dài một khoảng thời gian
    VD:
    + 6h sáng hàng ngày, mỗi lần 10p
    + 5h sáng mỗi 2 ngày, mỗi lần 20p
  - Quãng thời gian cố định: => chuyển thành dạng 1
    VD:
    + Từ 5h tới 6h
    + Từ 22h tới 5h
  - Thời điểm cách quãng
    VD:
    + Mỗi 12h một lần, mỗi lần 10p
  - 
  """
  def __init__(self, begin=None, end=None, duration=None, time_range=None, every=None, fromz=None, toz=None, cycle=None):
    """
    - ``begin``: Thời gia bắt đầu
    - ``end``: Thời gian kết thúc
    - ``duration``: Thời gian kéo dài hoạt động
    - ``time_range``: Quãng thời gian xác định
    - ``every``: Lặp lại hoạt động mỗi 1 khoảng chu kỳ
    - ``fromz``, ``toz``: Giới hạn thời gian của ``every``
    - ``cycle``: Chu kỳ - Mặc định là 1 ngày


    --------------------------------------------

    Có 3 cách định giờ chính:
    ---

    - [1] Bắt đầu tại một thời điểm xác định và kéo dài trong 1 khoảng thời gian:
      VD 1: Bắt đầu lúc 6h30 và 18h hàng ngày, mỗi lần 30 phút
      { "begin": ["6:30", "18:00"], "duration": "30" } (*) Dạng rõ ràng
      { "begin": ["6:30-30", "18:00-30"] } (*) Dạng tích hợp duration
      { "begin": ["6:30-30", "18:00-30"], "duration": "30" } (*) duration ở trong "begin" có ưu tiên cao hơn
      VD 2: Bắt đầu lúc 6h30 và 18h mỗi 3 ngày, mỗi lần 30 phút
      { "begin": ["6:30", "18:00"], "duration": "30", "cycle": "3:0:0:0" } (*) Dạng rõ ràng
      { "begin": ["6:30-30", "18:00-30"], "cycle": "3:0:0:0" } (*) Dạng tích hợp duration
      { "begin": ["6:30~3:0:0:0", "18:00~3:0:0:0"], "duration": "30" } (*) Dạng tích hợp cycle
      { "begin": ["6:30-30~3:0:0:0", "18:00-30~3:0:0:0"] } (*) Dạng tích hợp cả duration và cycle
      { "begin": ["6:30-30~3:0:0:0", "18:00-30~3:0:0:0"], "duration": "30", "cycle": "3:0:0:0" } (*) duration và cycle ở trong có ưu tiên cao hơn

    - [2] Bắt đầu và kết thúc tại các thời điểm xác định (Không có duration)
      VD 1: Thực hiện vào lúc từ 6h30 tới 7h và 18h tới 18h30 hàng ngày
      { "time_range": ["6:30-7:00", "18:00-18:30"] }
      VD 2: Thực hiện vào lúc từ 6h30 tới 7h mỗi và 18h tới 18h30 mỗi 3 ngày
      { "time_range": ["6:30-7:00", "18:00-18:30"], "cycle": "3:0:0:0" }
      { "time_range": ["6:30-7:00~3:0:0:0", "18:00-18:30~3:0:0:0"] } (*) Dạng tích hợp
      VD 3: Thực hiện vào lúc từ 6h30 tới 7h mỗi 2 ngày và 18h tới 18h30 mỗi 3 ngày
      { "time_range": ["6:30-7:00~2:0:0:0", "18:00-18:30~3:0:0:0"] }
      { "time_range": ["6:30-7:00~2:0:0:0", "18:00-18:30~3:0:0:0"], "cycle": "3:0:0:0" } (*) cycle ở trong "time_range" có ưu tiên cao hơn

    - [3] Thực hiện hành động lặp lại theo chu kỳ ngắn
      VD 1: Mỗi 1h thực hiện 15 phút và mỗi 6h thực hiện 15 phút
      { "every": ["1:00", "6:00"], "duration": "15" }
      VD 1: Mỗi 1h thực hiện 15 phút và mỗi 6h thực hiện 30 phút
      { "every": ["1:00-15", "6:00-30"] } (*) Dạng tích hợp duration
      VD 2: Chu kỳ như VD 1 nhưng được giới hạn thời gian trong 1 khoảng từ 18h tới 6h sáng ngày hôm sau
      { "every": ["1:00", "6:00"], "duration": "15", "from": "18:00", "to": "6:00" } (*) Dạng rõ ràng
      { "every": ["1:00-15", "6:00-30"], "from": "18:00", "to": "6:00" } (*) Dạng tích hợp duration
      { "every": ["1:00|18:00-6:00", "6:00|18:00-6:00"], "duration": "15" } (*) Dạng tích hợp limit
      { "every": ["1:00-15|18:00-6:00", "6:00-30|18:00-6:00"] } (*) Dạng tích hợp duration và limit
      VD 3: Chu kỳ như VD 1 nhưng được giới hạn thời gian trong 1 khoảng từ 18h tới 6h sáng ngày hôm sau mỗi 3 ngày. Cycle này là dành cho from-to, tức là mỗi 3 ngày thì mới thực hiện hành động từ 18h tới 6h sáng theo chu kỳ.
      { "every": ["1:00", "6:00"], "duration": "15", "from": "18:00", "to": "6:00", "cycle": "1:0:0:0" } (*) Dạng rõ ràng
      { "every": ["1:00-15", "6:00-15"], "from": "18:00", "to": "6:00", "cycle": "1:0:0:0" } (*) Dạng tích hợp duration
      { "every": ["1:00-15|18:00-6:00", "6:00-15|18:00-6:00"], "cycle": "1:0:0:0" } (*) Dạng tích hợp duration, limit
      { "every": ["1:00-15|18:00-6:00~1:0:0:0", "6:00-15|18:00-6:00~1:0:0:0"] } (*) Dạng tích hợp duration, limit và cycle

    - [4] Tích hợp dạng [1] và [2]
      { "begin": ["6:30", "18:00"], "duration": "30", "time_range": ["6:30-7:00", "18:00-18:30"] }
    """
    
    self.begin = self._end = self.duration = self.every = self.cycle = self.time_range = None
    self.duration = TimeAction.parse_timedelta(duration) if duration else timedelta(days=0)
    self.cycle = TimeAction.parse_timedelta(cycle) if cycle else timedelta(days=1)

    ### Kiểm tra "begin" cho dạng thời điểm xác định và kéo dài trong 1 khoảng
    if begin:
      # Kiểm tra ghi đè cycle
      split_c = begin.split('~')
      if len(split_c) > 1:
        self.cycle = TimeAction.parse_timedelta(split_c[1])
        begin = split_c[0]
      # Kiểm tra ghi đè duration
      split_d = begin.split('-')
      if len(split_d) > 1:
        self.duration = TimeAction.parse_timedelta(split_d[1])
        begin = split_d[0]
      # Lấy thời gian bắt đầu
      if len(begin) > 0:
        self.begin = TimeAction.parse_timedelta(begin)

    ### Kiểm tra "time_range" cho dạng trong khoảng thời gian xác định
    if time_range != None:  # 6:30-7:00~2:0:0:0
      # Kiểm tra ghi đè cycle
      split_c = time_range.split('~')
      if len(split_c) > 1:
        self.cycle = TimeAction.parse_timedelta(split_c[1])
        time_range = split_c[0]
      # Lấy khoảng thời gian hành động
      split_r = time_range.split('-')
      if len(split_r) == 2:
        begin = TimeAction.parse_timedelta(split_r[0])
        end = TimeAction.parse_timedelta(split_r[1])
        if end < begin:
          end += timedelta(days=1)
        self.begin = begin
        self.duration = end - begin

    ### Hành động cách nhau một quãng thời gian theo chu kỳ
    if fromz: self.begin = TimeAction.parse_timedelta(fromz)
    if toz: self._end = TimeAction.parse_timedelta(toz)
    if every != None: # 1:00-15|18:00-6:00~1:0:0:0
      # Kiểm tra ghi đè cycle
      split_c = every.split('~')
      if len(split_c) > 1:
        self.cycle = TimeAction.parse_timedelta(split_c[1])
        every = split_c[0]
      # Kiểm tra ghi đè giới hạn
      split_l = every.split('|')
      if len(split_l) > 1:
        fromz, toz = split_l[1].split('-')
        self.begin = TimeAction.parse_timedelta(fromz)
        self._end = TimeAction.parse_timedelta(toz)
        if self._end < self.begin:
          self._end += timedelta(days=1)
        every = split_l[0]
      # Kiểm tra ghi đè duration
      split_d = every.split('-')
      if len(split_d) > 1:
        self.duration = TimeAction.parse_timedelta(split_d[1])
        every = split_d[0]
      self.every = TimeAction.parse_timedelta(every)

  @property
  def end(self):
    return self._end if self._end else self.begin + self.duration
  
  @staticmethod
  def parse_timedelta(time):
    """ Chuyển thời gian dạng string sang timedelta để tính toán
      - 1 số:              phút
      - 2 số:        giờ : phút
      - 3 số:        giờ : phút : giây
      - 4 số: ngày : giờ : phút : giây
    """
    days, hours, minutes, seconds = 0,0,0,0
    parts = time.replace(' ', '').split(':')
    parts = list(map(lambda x: int(x), parts))
    if   len(parts) == 1:              minutes          = parts[0]
    elif len(parts) == 2:       hours, minutes          = parts
    elif len(parts) == 3:       hours, minutes, seconds = parts
    elif len(parts) == 4: days, hours, minutes, seconds = parts
    return timedelta(days=days, hours=hours, minutes=minutes, seconds=seconds)

  def check_time(self, start=None, now=None):
    if start == None:
      start = timedelta()
    if now == None:
      now = datetime.datetime.now()
    dif = now - start

    # Tính toán lại theo cycle
    dif = dif % self.cycle

    pass_every = pass_time = False
    # Kiểm tra every
    if self.every:
      pass_every = dif % self.every > self.duration # Kiểm tra nằm trong khoảng hành động của chu kỳ không
      pass_every = pass_every and ((self.begin and self.end and self.begin <= dif < self.end) or not self.begin or not self.end) # Kiểm tra nằm trong khoảng giới hạn không
    else:
      # Kiểm tra bắt đầu tại một thời điểm xác định
      if self.begin:
        pass_time = self.begin <= dif < self.end

    if pass_every:
      from_time = now - (dif % self.every)
      to_time = from_time + self.duration
    elif pass_time:
      from_time = now + self.begin
      to_time = from_time + self.duration
    
    if pass_every or pass_time:
      return {"from": from_time, "to": to_time, "remaining": to_time - now}

    return False

class TimeActionGroup:
  """ Nhóm các TimeAction lại với nhau cho một hành động
  - Giúp chuyển đổi từ dạng string kết hợp sang các đối tượng TimeAction riêng biệt
  VD:
  "6:20, 17:20" sẽ chuyển thành 2 đối tượng TimeAction có cùng duration
  """
  def __init__(self, time_info=None):
    self.time_actions = []
    self.append_time(time_info)
  
  @staticmethod
  def are_you(signal):
    return 'begin' in signal or 'time_range' in signal or 'every' in signal
  
  def append_time(self, info):
    if not info: return
    begin = info['begin'] if 'begin' in info else None
    time_range = info['time_range'] if 'time_range' in info else None
    every = info['every'] if 'every' in info else None
    duration = info['duration'] if 'duration' in info else None
    cycle = info['cycle'] if 'cycle' in info else None
    fromz = info['from'] if 'from' in info else None
    toz = info['to'] if 'to' in info else None

    if begin: # Đặt giờ theo mốc thời gian xác định, mỗi lần kéo dài 1 khoảng
      if isinstance(begin, list):
        for t in begin:
          self.time_actions.append(TimeAction(begin=t, duration=duration, cycle=cycle))

    if time_range: # Đặt giờ theo khoảng thời gian bắt đầu và kết thúc xác định
      if isinstance(time_range, list):
        for t in time_range:
          self.time_actions.append(TimeAction(time_range=t, cycle=cycle))

    if every: # Đặt giờ theo cách quãng thời gian, mỗi lần kéo dài 1 khoảng
      if isinstance(every, list):
        for t in every:
          self.time_actions.append(TimeAction(every=t, duration=duration, fromz=fromz, toz=toz, cycle=cycle))

  def append_group(self, time_action_group):
    try:
      if 'list' in type(time_action_group.time_actions):
        self.time_actions += time_action_group.time_actions
    except:
      pass

  def check_time(self, start=None):
    now = datetime.datetime.now()
    for time_action in self.time_actions:
      check_result = time_action.check_time(start=start, now=now)
      if check_result:
        return {"type": "timing", "detail": check_result}
    return False