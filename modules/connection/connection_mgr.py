# coding=utf-8

# import modules.connection.blue_services as blue
from modules.connection.LAN import LANCtl
from modules.connection.websocket import WebSocketCtl
from modules.connection.socket_io_client import SocketIOClientCtl
from modules.connection.security import Security

import struct
from time import time, sleep
import json

class ConnectionManager:
  """#### Package structure
  
  | START |    Security   | DELM |            Header           | DELM | Data | END_SIGN |
  |-------|---------------|------|-----+-----------+-----------|------|------|----------|
  | \xfd  | security_info | \xfe | cmd |   sub 1   |   sub 2   | \xfe | Data |   \xff   |
  |  1b   |       nb      |      | 1b  |    1b     |    1b     |      |  nb  |    1b    |
  [Default CMD is \xf4]                                                                  

  """
  header_length = 3
  _default_cmd = 255

  def __init__(self, host="0.0.0.0", websocket_port=4444, lan_port=4445):
    self.LANctl = LANCtl(host, lan_port, self.on_request, self)
    # self.bluetooth_handle = blue.BluetoothService(self.on_request)
    self.websocketCtl = WebSocketCtl(host, websocket_port, self.on_request)
    self.socketIOClient = SocketIOClientCtl(on_request=self.on_request)
    self._last_send = 0
    self.handle_mapping = { }
    self.security = Security()
    self.register_request_handle(1, self.hand_shake_handle)
    self.config_package()

  def config_package(self, start=b'\xfd', end=b'\xff', delimiter=b'\xfe', defcmd=b'\xf4'):
    """ Định nghĩa một số chốt trong cấu trúc gói tin. Defcmd = 244. """
    self.strPack = { "start": chr(start[0]), "end": chr(end[0]), "delimiter": chr(delimiter[0]), "defcmd": defcmd[0], "none": '', "cmd": lambda c: ord(c), "str": lambda s: s, "parse": lambda p: p.decode("utf-8") }
    self.bPack = { "start": start, "end": end, "delimiter": delimiter, "defcmd": defcmd, "none": b'', "cmd": lambda c: c, "str": lambda s: s.decode('utf-8'), "parse": lambda p: bytes(p, "ascii") }
  
  def get_package_config(self, data):
    return self.strPack if isinstance(data, str) else self.bPack
  
  @property
  def mobile_clients(self):
    return self.websocketCtl.clients
  
  def attach_garden_info(self, garden_info):
    self.garden_info = garden_info
  
  @property
  def host(self):
    return self.websocketCtl.host

  def register_request_handle(self, cmd, handle=None):
    if handle is not None:
      self.handle_mapping[cmd] = handle

  def on_request(self, request, client=None):
    package_2_send = None
    while True:
      (security_info, cmd, sub1, sub2, msg, rest) = self.resolve_package(request)
      if security_info is None or cmd is None: # Dữ liệu nhận được không hợp lệ
        return (rest, package_2_send)

      if len(security_info) == self.security.token_length: # Kiểm tra mã bảo vệ
        if not self.security.check_token(security_info):
          package = self.build(cmd, sub1, sub2, 'Invalid Token')
          if package != None:
            package_2_send = package_2_send + package if package_2_send != None else package
        else:
          if cmd is 1 and sub1 is 1:  # Nhánh 1-1: refresh token
            self.security.tokens.pop(security_info)
          package = self._dispath_request(cmd, sub1, sub2, msg, client)
          if package != None:
            package_2_send = package_2_send + package if package_2_send != None else package
      else: # Nếu vườn không được bảo vệ
        if not self.security.check_security_code(security_info):
          package = self.build(cmd, sub1, sub2, 'Invalid Security Code')
          if package != None:
            package_2_send = package_2_send + package if package_2_send != None else package
        else:
          package = self._dispath_request(cmd, sub1, sub2, msg, client)
          if package != None:
            package_2_send = package_2_send + package if package_2_send != None else package

      if len(rest) > 0: # Dữ liệu nhận được còn dư, tiếp tục xử lý (nhận hơn 1 gói tin)
        request = rest
      else:             # Dữ liệu nhận được vừa đủ 1 gói tin
        return (rest, package_2_send)
  
  def _dispath_request(self, cmd, sub1=None, sub2=None, msg=b'', client=None):
    """ Đưa thông điệp qua hàng đợi sự kiện xem có nào xử lý thông điệp không """
    pkgcfg = self.get_package_config(msg)
    sub1 = sub1 if sub1 != None else pkgcfg['defcmd']
    sub2 = sub2 if sub2 != None else pkgcfg['defcmd']
    package_2_send = None
    if cmd in self.handle_mapping: # Nếu sự kiện được cài để xử lý
      package_2_send = self.handle_mapping[cmd](cmd, sub1, sub2, msg, client)
    return package_2_send

  def run(self):
    self.LANctl.run()
    # self.bluetooth_handle.run()
    self.websocketCtl.run()
    self.socketIOClient.run()

  def build(self, cmd, sub1=None, sub2=None, data_2_send=''):
    if data_2_send == None: return None
    pkgcfg = self.get_package_config(data_2_send)
    sub1 = sub1 if sub1 != None else pkgcfg['defcmd']
    sub2 = sub2 if sub2 != None else pkgcfg['defcmd']
    header = self.build_header(cmd, sub1, sub2, isinstance(data_2_send, str))
    # START_HEADERS_D_DATA_END
    package = pkgcfg['start'] + header + pkgcfg['delimiter'] + data_2_send + pkgcfg['end']
    return package
  
  def pack(self, cmd, sub1=None, sub2=None, data_2_send=''): # alias for build
    return self.build(cmd, sub1=None, sub2=None, data_2_send='')

  def build_header(self, cmd, sub1=None, sub2=None, is_str=True):
    pkgcfg = self.get_package_config('')
    sub1 = sub1 if sub1 != None else pkgcfg['defcmd']
    sub2 = sub2 if sub2 != None else pkgcfg['defcmd']
    return "{}{}{}".format(chr(cmd), chr(sub1), chr(sub2)) if is_str else bytes([cmd,sub1,sub2])
  
  def resolve_package(self, data):
    """ Phân giải gói tin thành (security_info, cmd, sub1, sub2, data, rest). """
    pkgcfg = self.get_package_config(data)

    default = [None, None, None, None, pkgcfg['none'], data]

    try:
      packages = data.split(pkgcfg['end'])
      if len(packages) < 2: # Nếu không có end signal
        return default
    except:
      return default

    package = packages[0] # pkg: START_SEC_D_HEAD_D_DATA(not END)
    if pkgcfg['start'] not in package:
      return [None, None, None, None, pkgcfg['none'], data[len(package) + len(pkgcfg['end']):]]

    # Kiểm tra gói tin có đủ các thành phần khác không
    if len(package) < 6:
      return default

    rest = data[len(package) + len(pkgcfg['end']):]
    package = package[len(pkgcfg['start']):]
    try:
      security_info, header, data = package.split(pkgcfg['delimiter'])
    except:
      return default
    cmd = pkgcfg['cmd'](header[0])  # Chuyển về giá trị số
    sub1 = pkgcfg['cmd'](header[1])
    sub2 = pkgcfg['cmd'](header[2])
    security_info = pkgcfg['str'](security_info) # chuyển về giá trị kiểu string
    print("[Conn - Manager] > package: {}".format((security_info, cmd, sub1, sub2, data, rest)))
    return (security_info, cmd, sub1, sub2, data, rest)

  def connectWifi(self, SSID, password):
    pass

  def hand_shake_handle(self, cmd, sub1, sub2, data, client):
    # cmd is 1
    if sub1 is 1: # [Get Access Token]
      if self.security.isEnable:
        token = self.security.generate_token()
        self.security.allow_token(token)
        package = token
      else:
        package = ''
    elif sub1 is 2: # [ Change Security Code ]
      old_securit_code, securit_code = json.loads(data)
      if self.security.check_security_code(old_securit_code):
        self.security.save_security_code(securit_code)
        package = 'OK'
      else:
        package = "Wrong_Security_Code"
    elif sub1 is 3: # [Config wifi]
      if sub2 is 1: # Stage 1: retrieve wifi list to let user choose
        package = json.dumps([
          {"SSID": "Minh Thai", "Pass": "nhatrang9x"},
          {"SSID": "IUHYRA", "Pass": "iuhyra@123"},
          {"SSID": "moidoiten", "Pass": "passla123"}
        ], ensure_ascii=False)
      elif sub2 is 2: # Stage 2: User choose one and send password of that wifi to us. Then we connect and resend the host.
        SSID, password = data.split(":")
        self.connectWifi(SSID, password)
        package = "{}".format(self.host)
    return self.build(cmd, sub1, sub2, package)



  #########################################################################
  #           Một số phương thức hỗ trợ kết nối đến Cloud Server          #
  #########################################################################

  def send2Cloud(self, data):
    self.socketIOClient.send(data)

  def mustSend2Cloud(self, data):
    self.socketIOClient.mustSend(data)


  def close(self):
    self.socketIOClient.close()
    self.LANctl.close()
    self.websocketCtl.close()
    print("[Conn - Manager] </> Connection Manager is Stopped!")