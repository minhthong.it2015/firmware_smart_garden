# coding=utf-8

import asyncio
import websockets
import ifcfg, platform
import socket
from time import sleep

class WebSocketCtl:
  def __init__(self, host="0.0.0.0", port=4444, on_request=None, garden_ip_signal=['192.168.244.']):
    self.port = port
    self.on_request = on_request
    self.clients = []
    self.server = None
    self.garden_ip_signal = garden_ip_signal

  @property
  def ipv4(self):
    try:
      ip = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2][::-1] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]
    except:
      ip = 'localhost'
    if ip == 'no IP found': ip = ''
    return ip
  
  @property
  def ip_garden(self):
    ip = self.ipv4
    if not ip or ip == 'localhost' or not [ip_signal for ip_signal in self.garden_ip_signal if ip_signal in ip]:
      try:
        ip = ''
        interfaces = ifcfg.interfaces()
        wifi_card = 'wlan0' if self.isRaspi else 'Wireless LAN adapter Wi-Fi'
        if wifi_card not in interfaces: ip = ''
        else: ip = interfaces[wifi_card]['inet4'][::-1][0]
      except:
        ip = ''
    return ip
  
  @property
  def ip_garden_or_v4(self):
    ip_garden, ipv4 = self.ip_garden, self.ipv4
    return ip_garden if ip_garden else ipv4

  @property
  def isRaspi(self):
    return 'Windows' not in platform.system()

  @property
  def host(self):
    return "{}:{}".format(self.ipv4, self.port)


  ##################################################
  #                 Class Methods                  #
  ##################################################
  
  def recv_message(self, socket, result):
    socket.waiting = False
    if not result._result: return
    socket.data += result._result
    while len(socket.data) > 0:
      socket.data, package_2_send = self.on_request(socket.data, socket)
      if package_2_send != None and len(package_2_send) > 0:
        try:
          asyncio.ensure_future(socket.send(package_2_send))
        except BaseException as err:
          socket.waiting = None
      else:
        break
  
  async def request_handle(self, socket, path):
    print("[Conn - WS Server] > Connection from {} (path: {}).".format(socket.remote_address, path))
    try:
      if not [x for x in self.clients if x.remote_address == socket.remote_address]:
        self.clients.append(socket)
      
      socket.waiting = False
      socket.data = ''
      while True:
        if socket.close_code or socket.waiting == None:
          raise websockets.ConnectionClosed(socket.close_code, socket.close_reason)
        if not socket.waiting:
          socket.waiting = True
          try:
            future = asyncio.ensure_future(socket.recv())
            future.add_done_callback(lambda rs: self.recv_message(socket, rs))
            await future
          except BaseException as err:
            raise err
        else:
          await asyncio.sleep(.01)
    except BaseException as err:
      if socket in self.clients:
        self.clients.remove(socket)
      print("[Conn - WS Server] > Client disconnected: {} ({}).".format(socket.remote_address, err))
      

  def run(self):
    print("[Conn - WS Server] <*> Websocket server is listening on {}:{}".format(self.ip_garden_or_v4, self.port))
    try:
      self.server = websockets.serve(self.request_handle, "0.0.0.0", self.port)
      self.server = asyncio.get_event_loop().run_until_complete(self.server)
      return True
    except BaseException as err:
      print("[Conn - WS Server] >> Failed to start server on {}:{} ({})".format(self.ip_garden_or_v4, self.port, err))
      return False


  def close(self):
    print("[Conn - WS Server] </> Websocket Server is closing...")
    asyncio.get_event_loop().run_until_complete(self.server.wait_closed())
    self.server.close()
    print("[Conn - WS Server] </> Websocket Server is Stopped!")