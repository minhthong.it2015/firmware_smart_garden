
# from socketIO_client import SocketIO, LoggingNamespace
from websocket import WebSocketApp
import threading
import asyncio
import json
from time import sleep

class SocketIOClientCtl:
  def __init__(self, host="localhost", port=80, protocol="ws", on_request=None):
    print("[Conn - SocketIO] >> Connecting to websocket server...")
    self._listeners = {}
    self._event_listeners = {} # Mảng lưu các listener
    self._event_listeners["ready"] = []

    self._callbackChannel = {}
    self.sendQueue = []
    self.host = host
    self.port = port
    self.protocol = protocol
    self.on_request = on_request
    self.connected = None
    self.die = False
    self.delayReconnect = 5

    self.attachEvents()
  
  def run(self):
    self._run()
  
  def _run(self):
    self.wsLoop = asyncio.get_event_loop()
    self.wsThread = threading.Thread(target=self.start, args=(self.wsLoop,))
    self.wsThread.start()

  def start(self, loop):
    asyncio.set_event_loop(loop)
    self._start()

  def _start(self):
    while True:
      if not self.connected:
        self.url = "{}://{}:{}/socket.io/?EIO=3&transport=websocket".format(self.protocol, self.host, self.port)
        self.socket = WebSocketApp(self.url, 
                        on_open=self._onopen, on_message=self._onmessage, on_error=self._onerror, on_close=self._onclose)
        self.socket.run_forever()
        sleep(self.delayReconnect)
      else:
        sleep(self.delayReconnect)
  
  def _onopen(self):
    self.connected = True
    print("[Conn - SocketIO] <*> Open connection!")
  
  def _onready(self):
    print("[Conn - SocketIO] <*> Ready")
    self.emit_event("ready", {})
    self.onReady()
  
  def _onmessage(self, msg):
    if msg == '3':
      pass
    elif msg == '40':
      self._onready()
    else:
      if msg[0] == '0':
        data = json.loads(msg[msg.find('{'):msg.rfind('}')+1])
        self._pingThread = threading.Thread(target=self._ping, args=(data['pingInterval'],))
        self._pingThread.start()
      else:
        code = msg[:msg.find('[')]
        data = json.loads(msg[msg.find('['):msg.rfind(']')+1])
        self._dispatchEvent(code, data)

  def _onerror(self, err):
    if self.die: return
    if err.errno == 10061 and (self.connected or self.connected is None):
      self.connected = False
      print("[Conn - SocketIO] <!> SocketIO Error: ", err)
    # self._start()

  def _onclose(self):
    if self.die: return
    if (self.connected or self.connected is None):
      self.connected = False
      print("[Conn - SocketIO] </> SocketIO Closed. Try to reconnect!")
    # self._start()

  def _ping(self, interval):
    while True:
      try:
        sleep(interval/1000)
        self.socket.send('2')
      except:
        pass

  def add_event_listener(self, event="", callback=None):
    """ event = "ready" """
    if not callback: return False
    if not event:
      for event in self._event_listeners:
        self._event_listeners[event].append(callback)
    else:
      if event not in self._event_listeners:
        return False
      self._event_listeners[event].append(callback)
    return True

  def emit_event(self, event_type, event_obj):
    for listener in self._event_listeners[event_type]:
      listener(event_obj)



  ###########################################################
  #                    Start Custom Methods                  
  ###########################################################

  def on(self, event, callback):
    if event not in self._listeners:
      self._listeners[event] = []
    self._listeners[event].append(callback)
  
  def _dispatchEvent(self, code, data):
    if len(code) >= 3:
      if code[2:] in self._callbackChannel:
        self._callbackChannel[code[2:]](data)
    elif data[0] in self._listeners:
      for callback in self._listeners[data[0]]:
        callback(data[0], data[1:])
    else:
      print("[Conn - SocketIO] > Event not handled: ", data)
  
  def emit(self, event, msg, callback=None, mustSend=False): # mustSend-False : Bỏ qua gói tin nếu lỗi mạng
    channel = ''
    if callback != None:
      for i in range(10): # Kiểm tra đã từng đặt callback đó vào channel nào chưa. Nếu có thì dùng lại
        if i in self._callbackChannel and callback == self._callbackChannel[channel]:
          channel = str(i)
          break
      else:
        for i in range(10):
          if i not in self._callbackChannel:
            channel = str(i)
            self._callbackChannel[channel] = callback
            break
    evt = '42' + channel if callback else '42'
    package = evt + json.dumps([ event, (msg if isinstance(msg, str) else json.dumps(msg)) ])
    try:
      self.socket.send(package)
    except Exception as err:
      if mustSend:
        self.sendQueue.append(package)
        self.sendQueue = self.sendQueue[-100:] # Giữ tối đa 100 gói tin
      print("[Conn - SocketIO] <!> Error: ", err)
  
  def send(self, msg):
    self.emit('data', msg, self._standardDataCallback)
  
  def mustSend(self, msg):
    self.emit('data', msg, self._standardDataCallback, True)


  ###############################################

  def onReady(self):
    for pkg in self.sendQueue:
      try:
        self.socket.send(pkg)
      except:
        pass
    self.sendQueue = []

  def attachEvents(self):
    self.on("data", self.standardDataHandler)
  
  def standardDataHandler(self, event, data):
    rest, package_2_send = self.on_request(data[0], self)
    if package_2_send != None and len(package_2_send) > 0:
      self.emit('data', package_2_send, self._standardDataCallback)
  
  def _standardDataCallback(self, data):
    self.standardDataHandler('data', data)


  ###############################################

  def close(self):
    self.die = True
    self.socket.close()
    self.wsThread.join()
    print("[SocketOI Client] </> SocketOI Client is Stopped!")