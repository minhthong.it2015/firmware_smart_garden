# coding=utf-8

"""
Quản lý kết nối thông qua LAN
"""
import ifcfg, platform
import socket
import struct
import asyncio
  
class LANCtl:
  def __init__(self, host="0.0.0.0", port=4445, on_request=None, conn_mgr=None, garden_ip_signal=['192.168.244.']):
    self.port = port
    self.on_request = on_request
    self.clients = []
    self.conn_mgr = conn_mgr
    self.garden_ip_signal = garden_ip_signal

  @property
  def ipv4(self):
    try:
      ip = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2][::-1] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]
    except:
      ip = 'localhost'
    if ip == 'no IP found': ip = ''
    return ip
  
  @property
  def ip_garden(self):
    ip = self.ipv4
    if not ip or ip == 'localhost' or not [ip_signal for ip_signal in self.garden_ip_signal if ip_signal in ip]:
      try:
        ip = ''
        interfaces = ifcfg.interfaces()
        wifi_card = 'wlan0' if self.isRaspi else 'Wireless LAN adapter Wi-Fi'
        if wifi_card not in interfaces: ip = ''
        else: ip = interfaces[wifi_card]['inet4'][::-1][0]
      except:
        ip = ''
    return ip
  
  @property
  def ip_garden_or_v4(self):
    ip_garden, ipv4 = self.ip_garden, self.ipv4
    return ip_garden if ip_garden else ipv4

  @property
  def isRaspi(self):
    return 'Windows' not in platform.system()

  @property
  def host(self):
    return "{}:{}".format(self.ip_garden_or_v4, self.port)

  def run(self):
    loop = asyncio.get_event_loop()
    self.coro = asyncio.start_server(self.request_handle, "0.0.0.0", self.port, loop=loop)
    self.server = loop.run_until_complete(self.coro)
    print("[Conn - LAN] <*> LAN Server is listening on {}:{}".format(self.ip_garden_or_v4, self.port))
  
  def recv_message(self, reader, writer, result):
    reader.waiting = False
    if not result._result: return
    reader.data += result._result
    while len(reader.data) > 0:
      reader.data, package_2_send = self.on_request(reader.data, writer)
      if package_2_send != None and len(package_2_send) > 0:
        try:
          writer.write(package_2_send)
          asyncio.ensure_future(writer.drain())
        except:
          socket.waiting = None
      else:
        break
  
  async def request_handle(self, reader, writer):
    writer.addr = writer.get_extra_info('peername')
    print("[Conn - LAN] > Connection from {}".format(writer.addr))
    try:
      if not [w for w in self.clients if w.addr == writer.addr]:
        self.clients.append(writer)
        writer.send = lambda cmd,sub1=None,sub2=None,msg=b'': self.send(writer, cmd, sub1, sub2, msg)
      
      reader.waiting = False
      reader.data = b''
      while True:
        if reader._transport._closing or reader.waiting == None:
          raise Exception("Connection closed.")
        if not reader.waiting:
          reader.waiting = True
          # reader._result = await reader.read(100)
          # self.recv_message(reader, writer, reader)
          try:
            future = asyncio.ensure_future(reader.read(1024))
            future.add_done_callback(lambda rs: self.recv_message(reader, writer, rs))
            await future
          except BaseException as err:
            raise err
        else:
          await asyncio.sleep(.01)
    except BaseException as err:
      try: writer.close()
      except: pass
      try: self.clients.remove(writer)
      except: pass
      print("[Conn - LAN] > Client disconnected: {} ({}).".format(writer.addr, err))
      try: writer.on_disconnect()
      except: pass
      pass

  def send(self, writer, cmd, sub1=None, sub2=None, msg=b'', pack=True, callback=None):
    """ Cung cấp thêm một phương thức để gửi phản hồi từ bất kỳ đâu.
    ``callback``: callback(cmd, sub1, sub2, rs, msg)
    package = rs._result
    """
    if not isinstance(msg, bytes):
      msg = bytes(msg, "utf-8")
    package = self.conn_mgr.build(cmd, sub1, sub2, msg) if pack else msg
    try:
      print("[Conn - LAN] > Send to {}: {} ({})".format(writer.id, package, writer.addr))
    except:
      print("[Conn - LAN] > Send to {}: {}".format(writer.addr, package))
    try:
      writer.write(package)
      future = asyncio.ensure_future(writer.drain())
      future.add_done_callback(lambda rs: self.on_sent(cmd,sub1,sub2,rs,msg,callback))
    except:
      try:
        print("[Conn - LAN] > Failed to send {} to {} ({})".format(msg, writer.id, writer.addr))
      except:
        print("[Conn - LAN] > Failed to send {} to {}".format(msg, writer.addr))
      try: writer.close()
      except: pass
      try: self.clients.remove(writer)
      except: pass
  
  def on_sent(self, cmd, sub1, sub2, rs, msg, callback):
    print("[Conn - LAN] > sent done! {}".format(rs))
    if callback:
      callback(cmd, sub1, sub2, rs, msg)

  def close(self):
    print("[Conn - LAN] </> LAN Server is closing...")
    asyncio.get_event_loop().run_until_complete(self.server.wait_closed())
    self.server.close()
    print("[Conn - LAN] </> LAN Server is Stopped!")