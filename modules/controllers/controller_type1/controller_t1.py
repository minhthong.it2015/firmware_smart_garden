

from modules.controllers.controller_type1.gardener_t1 import GardenerType1
from modules.connection.connection_mgr import ConnectionManager

from modules.sync.mobile_sync import MobileSync
from modules.sync.station_sync_t1 import StationSyncType1
from modules.sync.cloud_sync_t1 import CloudSyncType1

import asyncio

class ControllerType1:
  def __init__(self):
    self.gardener = GardenerType1()

    # ConnectionManager quản lý truyền nhận thông điệp
    self.connMgr = ConnectionManager()

    # MobileSync gắn các sự kiện xử lý gói tin vào ConnectionManager
    self.mobileServices = MobileSync(self.connMgr, self.gardener)
    self.stationServices = StationSyncType1(self.connMgr, self.gardener)
    self.cloudServices = CloudSyncType1(self.connMgr, self.gardener)

  def run(self):
    try:
      self.connMgr.run()          # Bắt đầu khởi tạo các giao thức kết nối
      self.mobileServices.run()   # Bắt đầu dịch vụ đồng bộ mobile
      self.stationServices.run()  # Bắt đầu dịch vụ đồng bộ trong vườn
      self.cloudServices.run()    # Bắt đầu dịch vụ đồng bộ với cloud server
      self.gardener.work()        # Bắt đầu quá trình quản lý vườn
      asyncio.get_event_loop().run_forever()
    except KeyboardInterrupt:
      pass
    except BaseException as err:
      print("[System - Controller] <!> Error: ", err)
    finally:
      print("[System - Controller] > System Shuting down...")
      self.mobileServices.close()   # Ngắt dịch vụ đồng bộ mobile
      self.stationServices.close()  # Ngắt dịch vụ đồng bộ trong vườn
      self.cloudServices.close()    # Ngắt dịch vụ đồng bộ với cloud server
      self.gardener.stop()          # Ngắt quá trình quản lý vườn
      self.connMgr.close()          # Ngắt các giao thức kết nối
      asyncio.get_event_loop().close()
      print("[System - Controller] > Bye~...")