

from modules.plant.plant_lib.plant_lib_mgr import PlantLibraryManager
from modules.configuring.configs_mgr import ConfigManager
from modules.controllers.controller_type1.user_plant_mgr_t1 import UserPlantManager
from modules.controllers.controller_type1.stations_mgr_t1 import StationManager
from modules.equipment.equipments_mgr import EquipmentManager

from colorama import init
from colorama import Fore, Back, Style
init(autoreset=True)

class GardenerType1:
  def __init__(self):
    self.loadGardenID()
    self.cfgPath = ConfigManager('SystemPath')
    self.plant_lib_mgr = PlantLibraryManager(self.cfgPath.getc('plants_lib_path'))
    self.user_plant_mgr = UserPlantManager(self.cfgPath.getc('plants_path'), self.plant_lib_mgr)

    self.station_mgr = StationManager(self.user_plant_mgr,\
      stations_file_path = self.cfgPath.getc('stations_path'),\
      config_file_path = self.cfgPath.getc('stations_cfg_path'))

    self.equip_mgr = EquipmentManager(self.station_mgr)
    # self.user_plant_mgr.attach_station_mgr()

    pass # breakpoint -> does not remove this

  def work(self):
    print("[System - Gardener] <*> Gardener Start Working.")
    self.station_mgr.run()

  def dump(self):
    """ Lấy thông tin toàn vườn """
    # equipments = self.equip_mgr.dump()
    # user_plants = self.user_plant_mgr.dump()
    return {
      "stations": self.station_mgr.dump()
    }

  def dump_layer_1(self):
    """ Mobile sync - Lấy danh sách trạm và danh sách cây trồng trên trạm. """
    pass

  def loadGardenID(self):
    with open('./configs/id.lock', 'r+') as fp:
      self.gardenID = fp.read()

  def saveGardenID(self, newGardenID):
    with open('./configs/id.lock', 'w+') as fp:
      fp.write(newGardenID)
      fp.close()
    self.gardenID = newGardenID



  ####################################################################
  #            Phần xử lý cung cấp dữ liệu cho Mobile App            #
  ####################################################################
  
  def get_records_from(self):
    """ Mobile Sync - Lấy dữ liệu nhật ký hoạt động. """
    return []
  
  def get_station_list(self):
    """ Lấy thông tin tất cả trạm cây trong vườn. """
    return self.station_mgr.dump()
  
  def get_station_info(self, station_id):
    """ Mobile Sync - Lấy thông tin trạm (tt trạm, cây, trạng thái thiết bị) """
    return self.station_mgr.get_station_by_id(station_id).dump()
    
  def command_handle(self, station_id, equipment_role, state):
    """ Mobile Sync - Điều khiển trạm trực tiếp từ mobile """
    selected_station = self.station_mgr.get_station_by_id(station_id)
    if selected_station:
      print("[System - Gardener] > User Set {} to {} on station {}".format(equipment_role, state, station_id))
      selected_station.switch(equipment_role, state, "user_set")

  def executeCommand(self, command):
    print(Back.WHITE + Fore.BLUE + str(command))
    # print(Fore.RED + 'some red text')
    # print(Back.GREEN + 'and with a green background')
    # print(Style.DIM + 'and in dim text')
    # print(Style.RESET_ALL)
    # self.equip_mgr.equipments[2].on()

  def planting_new_plant(self, station_id, plant_info):
    """ Trồng cây trồng mới lên trên trạm """
    pass
  

  
  ############################################

  def stop(self):
    print("[System - Gardener] </> Gardener stopping...")
    self.station_mgr.stop()
    self.equip_mgr.stop()
    print("[System - Gardener] </> Gardener is Stopped!")