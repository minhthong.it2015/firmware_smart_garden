#coding=utf-8

from modules.plant.models.user_plant import UserPlant
from modules.tracker.observer import FileModifiedTracker

import json, shutil, os


class UserPlantManager:
  """ Quản lý tất cả cây trong vườn. Cây sau đó có thể được phân bố cho các trụ, giàn... """
  def __init__(self, plant_file_path='./assets/plants.json', plant_lib_mgr=None):
    self.plant_file_path = plant_file_path
    self.plant_lib_mgr = plant_lib_mgr
    self.plant_list = self.load_plants()

    self.plant_lib_mgr.add_event_listener(self.check_plant_lib_changes ,"change")
    self.tracking_changes()
  
  def load_plants(self, plant_file_path=''):
    self.plant_file_path = plant_file_path if plant_file_path else self.plant_file_path
    plant_list = []
    try:
      with open(self.plant_file_path, 'r', encoding='utf-8') as fp:
        plants = json.load(fp)
        for plant in plants:
          plant_list.append(UserPlant(json=plant, plant_lib_mgr=self.plant_lib_mgr))
      print("[System - Plant Manager] <*> Load User Plant: {} Plants.".format(len(plant_list)))
    except Exception as err:
      print("[System - Plant Manager] <!> Load user plants error. ({})".format(err))
    return plant_list
  
  def check_plant_lib_changes(self, event):
    print("[System - Plant Manager] <~> Check Plant Library Changes!")
    for plant in self.plant_list:
      plant.attach_plant_lib(plant_lib_mgr=event["target"])

  def get_plants_by_location(self, location):
    """ return dict object """
    plants = []
    for plant in self.plant_list:
      if plant.location == location:
        plants.append(plant)
    return plants

  def tracking_changes(self):
    self.tracker = FileModifiedTracker()
    self.tracker.track(self.plant_file_path, self._on_plant_list_change)
  
  def _on_plant_list_change(self, event):
    print("[System - Plant Manager] <~> Plants changes!")
    new_plants = self.load_plants(event.src_path)
    try:
      count_update = 0
      count_new = 0
      new_list = []
      for new_plant in new_plants:
        old_plants = list(filter(lambda plant: plant.id == new_plant.id, self.plant_list))
        if old_plants:
          count_update += len(old_plants)
          for plant in old_plants: plant.update(new_plant)
        else:
          new_list.append(new_plant)
          count_new += 1
      self.plant_list.extend(new_list)
      if count_new:
        pass
      print("[System - Plant Manager] > Updated User Plants! (+{} Plants / {} Updated)".format(count_new, count_update))
      # self.emit_event("change", {"target": self, "list": self.stations, "new": new_list})
    except Exception as err:
      print("[System - Plant Manager] <!> Failed to Update Plants: {}".format(err))

  def dump(self):
    """ return dict object """
    plants = []
    for plant in self.plant_list:
      plants.append(plant.dump())
    return plants
  
  def dump_plants_by_location(self, location):
    """ return dict object """
    plants = []
    for plant in self.plant_list:
      if plant.location == location:
        plants.append(plant.dump())
    return plants
  
  def create_new_plant(self):
    pass

  def save(self):
    tmp_file = "{}.tmp".format(self.plant_file_path)
    try:
      with open(tmp_file, "wt+") as fp:
        fp.write(json.dumps(self.dump(), ensure_ascii=False, indent=2))
        fp.close()
        shutil.copy(tmp_file, self.plant_file_path)
        os.remove(tmp_file)
    except BaseException as err:
      print("[System - Plant Manager] <!> Save Plant List error: {}".format(err))