# coding=utf-8

from modules.station.station import Station
from modules.tracker.observer import FileModifiedTracker
from modules.tracker.listenable import Listenable

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import asyncio, threading

import sys
import json
from time import sleep

class StationManager(Listenable):
  def __init__(self, user_plant_mgr, stations_file_path='./assets/stations.json', config_file_path='./configs/stations_cfg.cfg'):
    Listenable.__init__(self, ["stationschange"])
    self.user_plant_mgr = user_plant_mgr  # Quản lý của cây trồng
    self.is_ensuring = False  # Cờ báo hiệu đang tự động chăm sóc vườn
    
    self.stations_file_path = stations_file_path # Đường dẫn tới file lưu thông tin trạm
    self.stations = self.load_stations()  # Danh sách trạm
    
    self.tracking_changes()
  
  def load_stations(self, stations_file_path=''):
    """ ## Nạp danh sách trạm từ file lên\r\n
    Thông tin trạm: name, id, type, env_type, equipments
    """
    self.stations_file_path = stations_file_path if stations_file_path else self.stations_file_path
    try:
      with open(self.stations_file_path, 'r', encoding='utf-8') as fp:
        new_stations_json = json.load(fp)
        stations = []
        for station in new_stations_json:
          stations.append(Station(info=station, user_plant_mgr=self.user_plant_mgr))
        print("[System - Station Manager] <*> Station List loaded: {} stations.".format(len(stations)))
        return stations
    except Exception as err:
      print("[System - Station Manager] <*> Failed to load Station List: {}".format(err))
      return []
  
  def tracking_changes(self):
    self.tracker = FileModifiedTracker()
    self.tracker.track(self.stations_file_path, self._on_stations_changes)
  
  def _on_stations_changes(self, event):
    print("[System - Station Manager] <~> Stations changes!")
    new_stations = self.load_stations(event.src_path)
    try:
      has_new = 0
      new_list = []
      for new_station in new_stations:
        old_stations = list(filter(lambda station: station.id == new_station.id, self.stations))
        if old_stations:
          for station in old_stations: station.update(new_station)
        else:
          new_list.append(new_station)
          has_new += 1
      self.stations.extend(new_list)
      if has_new:
        pass
      print("[System - Station Manager] > Updated Stations! (+{} Stations)".format(has_new))
      self.dispatchEvent("stationschange", {"target": self, "data": self.stations, "new": new_list})
    except Exception as err:
      print("[System - Station Manager] <!> Failed to Update Stations: {}".format(err))

  def run(self):
    """ Bắt đầu chạy Station Manager """
    self.start_ensure_living_environment()



  ##############################################################
  #   Quản lý, đảm bảo môi trường sinh trưởng trên các trạm    #
  ##############################################################

  def _start_ensure_loop(self, loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()

  def _ensure_living_environment(self):
    print("[System - Station Manager] <*> Start ensure living enviroment on all station.")
    while self.is_ensuring:
      for station in self.stations:
        ## Synchonous style - Used for debug => does not remove this
        station.ensure_living_environment()
      sys.stdout.flush()
      sleep(1)
    print("[System - Station Manager] </> Stop ensure living enviroment on all station.")

  def start_ensure_living_environment(self):
    self.is_ensuring = True
    self.ensure_loop = asyncio.new_event_loop()

    self.ensure_thread = threading.Thread(target=self._start_ensure_loop, args=(self.ensure_loop,))
    self.ensure_thread.start()

    self.ensure_loop.call_soon_threadsafe(self._ensure_living_environment)
  
  def stop_ensure_living_environment(self):
    self.is_ensuring = False
    try:
      self.ensure_loop.stop()
      self.ensure_loop.close()
    except Exception as err:
      print("[System - Station Manager] > Error: ", err)
      pass


  ###########################################
  #       Quản lý định danh các trạm        #
  ###########################################

  def _generate_station_name(self):
    base_name = "Station"
    index = 1
    while True:
      for station in self.stations:
        if station.name == "{} {:02d}".format(base_name, index):
          break
      else:
        return "{} {:02d}".format(base_name, index)
      index += 1

  def attach_station(self, station_id):
    station = self.get_station_by_id(station_id)
    if station is not None:
      if not station.connected:
        station.connected = True
    else:
      new_station = Station(info={"id": station_id, "name": self._generate_station_name()},user_plant_mgr=self.user_plant_mgr)
      self.stations.append(new_station)
      self.save()
  
  def get_station_by_name(self, name):
    for station in self.stations:
      if station.name == name:
        return station
    return None

  def get_station_by_id(self, station_id):
    for station in self.stations:
      if station.id == station_id:
        return station
    return None
  
  def set_state(self, station_id, equipment, state, reason):
    station = self.get_station_by_id(station_id)
    station.set_state(equipment, state, reason)

  def planting_new_plant(self, station_id, plant_type, planting_date, alias):
    station = self.get_station_by_id(station_id)
    # station.plant_new_plant({"alias": alias,
    #                           "planting_date": planting_date,
    #                           "plant_type": plant_type,
    #                           "plant_id": self._generate_plant_id(plant_type)
    #                         }, self.plant_lib)
    self.save()

  def remove_plant(self, station_id, plant_id):
    station = self.get_station_by_id(station_id)
    station.remove_plant(plant_id)
    self.save()

  def _generate_plant_id(self, plant_type):
    signal = "{}-".format(plant_type)
    bigest_id = 0
    for station in self.stations:
      for plant in station.list_user_plant.plants:
        if signal in plant.plant_id:
          id = int(plant.plant_id.split('-')[1])
          if id > bigest_id:
            bigest_id = id
    return "{}-{}".format(plant_type, "000{}".format(bigest_id+1)[-3:])
  
  ############################################

  def dump(self):
    stations = []
    for station in self.stations:
      stations.append(station.dump())
    return stations

  def save(self):
    with open(self.stations_file_path, 'w', encoding='utf-8') as fp:
      stations = self.dump()
      for station in stations:
        station.pop('equipment_set')
      fp.write(json.dumps(stations, ensure_ascii=False, indent=2))

  def stop(self):
    print("[System - Station Manager] </> Station Manager is closing...")
    try: self.stop_ensure_living_environment()
    except: pass
    try: self.ensure_thread.join()
    except: pass
    print("[System - Station Manager] </> Station Manager is Stopped!")