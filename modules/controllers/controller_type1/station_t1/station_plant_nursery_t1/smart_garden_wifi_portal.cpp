
#include "smart_garden_wifi_ap.hpp"

/****************************************
 *                 Portal               *
 ****************************************/

String htmlPage =
    String("HTTP/1.1 200 OK\r\n") +
          "Content-Type: text/html\r\n" +
          "Connection: close\r\n" +  // the connection will be closed after completion of the response
          "Refresh: 5\r\n" +  // refresh the page automatically every 5 sec
          "\r\n" +
          "<!DOCTYPE HTML>" +
          "<html><meta charset=utf-8>" +
          "<div style='font-size: 30px; color: #b66e15; font-family:\"Segoe UI\", sans-serif'>Count: %d</div>" +
          "</html>";

void _wsOnEvent(unsigned char num, WStype_t type, unsigned char* payload, unsigned int lengthz)
{
  centralAP.wsOnEvent(num, type, payload, lengthz);
}


void SmartGardenWifiAP::startPortal() {
  portalServer.begin();
  Serial.printf("[Portal] > Open Portal on %s|%s:%d\r\n", getIP().c_str(), getLocalIP().c_str(), portalPort);

  wsPortal.begin();
  wsPortal.onEvent(_wsOnEvent);
  Serial.printf("[wsPortal] > Open Websocket Portal on %s|%s:%d\r\n", getIP().c_str(), getLocalIP().c_str(), wsPortalPort);
}

/**
 * Mở cổng thông tin để truy cập vào vườn
 */

void SmartGardenWifiAP::portalLoop() {
  wsPortal.loop();
  
  static String request = "";

  WiFiClient client = portalServer.available();
  if (client) {
    if (client.connected()) {
      Serial.printf("\r\n[Portal] >> Client connect: %s\r\n", client.remoteIP().toString().c_str());
      request = "";
      while (client.available()) {
        request += client.readStringUntil('\n');
      }
      resolvePortalRequest(request, client);
    }
    Serial.printf("[Portal] << Client passed: \r\n", client.remoteIP().toString().c_str());
  }

}

void SmartGardenWifiAP::wsOnEvent(unsigned char id, WStype_t type, unsigned char* _payload, unsigned int lengthz) {
  char *payload = (char*)_payload;
  switch (type) {
    case WStype_DISCONNECTED:             // if the websocket is disconnected
      Serial.printf("[wsPortal %u] Disconnected!\n", id);
      break;
    case WStype_CONNECTED:                // if a new websocket connection is established
      Serial.printf("[wsPortal %u] Connected from %s url: %s\n", id, wsPortal.remoteIP(id).toString().c_str(), payload);
      break;
    case WStype_TEXT:                     // if new text data is received
      Serial.printf("[wsPortal %u] get Text: %s\n", id, payload);
      if (!strcmp("GetRaspi", payload)) {
        wsPortal.sendTXT(id, raspiInfo);
      }
      break;
  }
}

void SmartGardenWifiAP::resolvePortalRequest(String request, WiFiClient client) {
  static int count = 0;
  if (request == "GetRaspi") {
    Serial.printf("[Portal] > Request Raspi Server Info: %s", raspiInfo.c_str());
    client.print(raspiInfo);
  } if (request.indexOf("raspi:") >= 0) { // Nhận được thông tin server từ raspi
    saveRaspiInfo(request);
    client.print("OK"); // Báo đã nhận được thông tin
  } else {
    if (request.indexOf("GET") >= 0) {
      Serial.printf("[[ Count: %d ]]\r\n", count);
      client.printf(htmlPage.c_str(), count++);
    }
  }
  Serial.println("[Portal] > Dump Request ------------------------");
  Serial.print(request);
  Serial.println("\r\n[Portal] > End of Dump Request -----------------");
}

bool SmartGardenWifiAP::portalContains(WiFiClient &client) {
  for (int i=0; i<4; i++) {
    if (clients[i].status() == ESTABLISHED
      && client.remoteIP() == clients[i].remoteIP()) return true;
  }
  return false;
}

void SmartGardenWifiAP::saveRaspiInfo(String info) {
  // info: raspi:192.168.44.100:4445
  raspiInfo = info.substring(6,999) + "\n";
  Serial.printf("[Portal] > Save Raspi Info: %s", raspiInfo.c_str());
  onLed2 // Bật đèn led thứ 2 nằm trên node MCU
}
