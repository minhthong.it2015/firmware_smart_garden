
#include "smart_garden_wifi_ap.hpp"

SmartGardenWifiAP centralAP = {};

void SmartGardenWifiAP::setup() {
  setupLed2 then offLed2
  WiFi.mode(WIFI_AP);
  broadcast();    // Khởi động mạng wifi vườn
//  testGoogle();   // Kiểm tra kết nối tới internet
  startPortal();  // Mở cổng thông tin trong vườn
}

void SmartGardenWifiAP::loop() {
  checkAndShowStations();
  portalLoop();
}

/**
 * Phát wifi ra toàn vườn
 */
void SmartGardenWifiAP::broadcast() {
  Serial.printf("[Wifi-AP] > Soft-AP Configuration: %s\r\n", WiFi.softAPConfig(APIP, gateway, subnet) ? "OK!" : "Error!");
  Serial.printf("[Wifi-AP] > Soft-AP Begin: %s\r\n", WiFi.softAP(ssid, password, channel) ? "OK!" : "Error!");
  
  Serial.printf("[Wifi-AP] > Soft-AP IP: %s\r\n", WiFi.softAPIP().toString().c_str());
}

bool SmartGardenWifiAP::testGoogle() {
  Serial.printf("[Wifi-STA] > Connecting to Wall-E: ");
  WiFi.begin("Wall-E", "Wastebasket");
//  WiFi.begin("IUHYRA", "iuhyra@12345");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("."); delay(500);
  }
  Serial.printf("\r\n[Wifi-STA] > Station IP address: %s\r\n", WiFi.localIP().toString().c_str());
  
  static bool couldConnected = false;
  if (!couldConnected) {
    int retry = 0;
    WiFiClient testClient;
    Serial.print("[Test] > Connecting to google (10s test): ");
    while (!testClient.connect("google.com", 80) && retry < 20) {
      Serial.print("."); delay(500);
      retry++;
    }
    if (testClient.connected()) {
      couldConnected = true;
      Serial.println("Connected to Google!");
    } else {
      Serial.println("Cannot connect to Google!");
    }
  }
  return couldConnected;
}

void SmartGardenWifiAP::checkAndShowStations() {
  static byte numClient = 0;
  if (WiFi.softAPgetStationNum() != numClient) {
    numClient = WiFi.softAPgetStationNum();
    Serial.printf("---> Num Clients: %d\r\n", numClient);
//    showStations();
  }
  showStations();
}

void SmartGardenWifiAP::showStations() {
  static String last = "";
  getStationsInfo();
  if (stationsInfo != last) {
    last = stationsInfo;
    Serial.print("----------- Stations ----------\r\n");
    Serial.print(stationsInfo);
    Serial.print("-------------------------------\r\n");
  }
}

String SmartGardenWifiAP::getStationsInfo() {
  struct station_info *stat_info;
  IPAddress address;
  stationsInfo = "";

  stat_info = wifi_softap_get_station_info();
  while (stat_info != NULL)
  {
    address = stat_info->ip.addr;
    stationsInfo += address.toString() + "\n";
    stat_info = STAILQ_NEXT(stat_info, next);
  }
  return stationsInfo;
}

