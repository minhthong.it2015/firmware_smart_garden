#pragma once
#ifndef _SMART_GARDEN_WIFI_AP_H
#define _SMART_GARDEN_WIFI_AP_H
extern "C" {
  #include  <user_interface.h>
}


#define builtinLed 2
#define setupLed pinMode(builtinLed, OUTPUT);
#define onLed digitalWrite(builtinLed, 0);
#define offLed digitalWrite(builtinLed, 1);
#define then 

#define builtinLed2 16
#define setupLed2 pinMode(builtinLed2, OUTPUT);
#define onLed2 digitalWrite(builtinLed2, 0);
#define offLed2 digitalWrite(builtinLed2, 1);
#define then 


//#include "connection.h"

#include <ESP8266WiFi.h>
#include <string.h>
#include <WebSocketsServer.h>

/**
 * Mỗi ESP được thiết lập để chỉ duy trì 1 kết nối duy nhất đến raspi server
 **/
class SmartGardenWifiAP {
  private:
//  Connection conn;
  public:
  IPAddress APIP = {192,168,244,1};
  IPAddress gateway = {192,168,244,1};
  IPAddress subnet = {255,255,255,0};

  const char ssid[32] = "Plant_Nursery_01";
  const char password[32] = "12345678";
  const int channel = 14;
  
  String stationsInfo = "";

  String raspiInfo = "\n";
  WiFiClient raspi;
  int raspiPortalPort = 4447;

  /****************************************
   *             Getter/Setter            *
   ****************************************/
  
  String getIP() { return WiFi.softAPIP().toString(); }
  String getLocalIP() { return WiFi.localIP().toString(); }



  /****************************************
   *             Class Methods            *
   ****************************************/

  void broadcast(); // Phát sóng, mạng ra toàn vườn
  bool testGoogle();
  
  
  void checkAndShowStations();
  String getStationsInfo();
  void showStations();

  void setup();
  void loop();

  /****************************************
   *                 Portal               *
   ****************************************/

  const int portalPort = 80;
  WiFiServer portalServer = {portalPort};  // Cổng thông tin để các station khác lấy thông tin mạng
  WiFiClient clients[4];
  const int wsPortalPort = 81;
  WebSocketsServer wsPortal = {wsPortalPort};  // Hỗ trợ websocket cho mobile
  
  void startPortal();
  void portalLoop();
  bool portalContains(WiFiClient &client);
  void resolvePortalRequest(String request, WiFiClient client);
  void saveRaspiInfo(String info);
  void wsOnEvent(unsigned char num, WStype_t type, unsigned char* payload, unsigned int lengthz);
};

extern SmartGardenWifiAP centralAP;

#endif

