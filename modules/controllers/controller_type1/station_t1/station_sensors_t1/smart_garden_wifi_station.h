

#ifndef SMART_GARDEN_WIFI_STA_H
#define SMART_GARDEN_WIFI_STA_H

void (*reset)(void) = 0;
void resetESP() { Serial.println("[Sys] </> ESP Refreshing..."); reset(); }

#include <ESP8266WiFi.h>
#include <string.h>
#include "connection.hpp"

#ifndef DEVICE_ID
#define DEVICE_ID "ESP_Device"
#endif

#ifndef CONTROL
#define CONTROL {}
#endif

#ifdef SENSOR_DHT22
#include "DHT.h"
#ifndef DHTTYPE
#define DHTTYPE DHT22
#endif
DHT dht(SENSOR_DHT22, DHTTYPE);
#endif

#ifdef SENSOR_BH1750
#include <Wire.h>
#include <BH1750.h>
BH1750 lightMeter(0x23);
#ifndef SDA
#define SDA 4
#endif
#ifndef SCL
#define SCL 5
#endif
#endif

#ifdef SENSOR_HC_SR501
int sensorPin = 12;    // select the input pin for the potentiometer
int ledPin = 2;      // select the pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor
#endif

/**********************************************************************************************************
 *                                         Hướng dẫn thiết đặt ESP                                        *
 * 
 * "#define DEVICE_ID" để xác định id của thiết bị
 * "#define CONTROL {pin1, pin2,...}" để thiết đặt là trạm này dùng để nhận lệnh điều khiển
 * "#define SENSOR_DHT22 <pin>" để thiết đặt là đọc dữ liệu cảm biến DHT22 trên <pin>
 * "#define SENSOR_BH1750 <pin>" để thiết đặt là đọc dữ liệu cảm biến BH1750 trên <pin>
 * "#define SENSOR_HC_SR501 <pin>" để thiết đặt là đọc dữ liệu cảm biến HC_SR501 trên <pin>
 * 
 **********************************************************************************************************/

#define builtinLed 2
#define setupLed pinMode(builtinLed, OUTPUT);
#define onLed digitalWrite(builtinLed, 0);
#define offLed digitalWrite(builtinLed, 1);
#define then 

#define builtinLed2 16
#define setupLed2 pinMode(builtinLed2, OUTPUT);
#define onLed2 digitalWrite(builtinLed2, 0);
#define offLed2 digitalWrite(builtinLed2, 1);
#define then 

class SmartGardenWifiSTA {
  private:
  Connection conn;
  public:
  String ssid;
  String password = "12345678";
  String gardenSignal = "Garden_";
  
  String portalServer = "";
  const uint16_t portalPort = 80;
  WiFiClient portal;
  
  String raspiServer;
  uint16_t raspiPort;
  WiFiClient raspi;

  String getDevicePackage() {
    return String("{\"id\": \"") + DEVICE_ID + "\"}";
  }

  byte equips[4] = CONTROL;
  
  void setup();
  void loop();

  String scanForGarden();
  bool connectToWifi(String ssidz, String passwordz);
  void connectToGarden();
  void gardenLoop();

  bool connectToPortal();

  bool retrieveRaspiServer();
  void connectToRaspiServer();
  void raspiServerLoop();
  
  void handleStation(Package &pkg);
  String get_station_values();

  #ifdef SENSOR_HC_SR501
  void trigger_detect_movement();
  #endif
};

void SmartGardenWifiSTA::setup() {
  #ifdef SENSOR_BH1750
  Wire.begin(SDA,SCL);
  lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE);
  #endif
  
  #ifdef CONTROL
  for (int i=0; i < sizeof(equips) && equips[i] > 0; i++) {
    pinMode(equips[i], OUTPUT);
    digitalWrite(equips[i], HIGH);
  }
  #endif

  WiFi.mode(WIFI_STA);
}

void SmartGardenWifiSTA::loop() {
  static unsigned long timer = millis();
//  digitalWrite(equips[0], 1);
  if (millis() - timer > 1800000) resetESP(); // Khởi động lại ESP mỗi 30p để tránh treo
  raspiServerLoop();
}

/** Kết nối đến một mạng wifi nào đó **/
bool SmartGardenWifiSTA::connectToWifi(String ssidz, String passwordz) {
  Serial.printf("[Garden] > Joining to: %s/%s", ssidz.c_str(), passwordz.c_str());
  WiFi.begin(ssidz.c_str(), passwordz.c_str());
  byte retry = 14;
  while (WiFi.status() != WL_CONNECTED) { // Kết nối trong 7s
    Serial.print("."); delay(500);
    if (--retry == 0) break;
  }
  if (WiFi.status() != WL_CONNECTED)
    Serial.printf("\r\n[Garden] > Failed -> %s:%s\r\n", WiFi.SSID().c_str(), WiFi.psk().c_str());
  else Serial.printf("\r\n[Garden] > Succeeded -> %s:%s\r\n", WiFi.SSID().c_str(), WiFi.psk().c_str());
  return WiFi.status() == WL_CONNECTED;
}



/******************************************************
 *                Station <-> Garden                  *
 ******************************************************/

/** Trả về SSID của vườn gần nhất. Nếu không tìm thấy sẽ tiếp tục quét */
String SmartGardenWifiSTA::scanForGarden() {
  byte retry = 0;
  while (true) {
    if (++retry > 5) resetESP();
    Serial.println("┌────────────────── Scan Result ──────────────────┐");
    int n = WiFi.scanNetworks();
    for (int i=0; i<n; ++i) {
      Serial.printf("├ [%02d] > %3d : %s\r\n", i+1, WiFi.RSSI(i), WiFi.SSID(i).c_str());
      if (WiFi.SSID(i).indexOf(gardenSignal) >= 0) {
        Serial.println("└────────────────────────────────────────────┘");
        return ssid = WiFi.SSID(i);
      }
    }
    Serial.println("└────────────────────────────────────────────┘");
  }
  return ssid;
}

void SmartGardenWifiSTA::connectToGarden() {
  if (WiFi.status() == WL_CONNECTED) return;

  // Thử kết nối tới wifi vườn gần đây nhất
  if (WiFi.SSID() != "" && connectToWifi(WiFi.SSID(), WiFi.psk())) return;

  // Nếu không được thử quét các mạng khác
  while (!connectToWifi(scanForGarden(), password));

  Serial.printf("[Garden] >>> Station Local IP: [%s]\r\n", WiFi.localIP().toString().c_str());
}


/******************************************************
 *                Station <-> Portal                  *
 ******************************************************/

bool SmartGardenWifiSTA::connectToPortal() {
  if (portal.connected()) return true;

  // use local IP to calculate portal IP
  IPAddress portalAddr = {WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], 1};
  
  Serial.printf("[Portal] > Connecting to Portal: %s:%d", portalAddr.toString().c_str(), portalPort);
  byte retry = 3;
  if (WiFi.status() != WL_CONNECTED) return false; // Kiểm tra lại kết nối mạng trước khi kết nối
  while (!portal.connect(portalAddr, portalPort)) { // Kết nối trong 3 lần
    // Kiểm tra lại kết nối mạng ngay khi lỗi
    if (WiFi.status() != WL_CONNECTED) { Serial.println(); return false; }
    Serial.print("."); delay(500);
    if (!--retry) break;
  }
  if (portal.connected()) Serial.printf("\r\n[Portal] > Portal OK!\r\n");
  else Serial.printf("\r\n[Portal] > Portal Error!\r\n");
  return portal.connected();
}

bool SmartGardenWifiSTA::retrieveRaspiServer() {
  static byte retryAll = 0;
  Serial.println("[Portal] > Retriving raspi IP...");
  do {
    if (++retryAll > 10) resetESP();
    
    if (WiFi.status() != WL_CONNECTED) return false; // Kiểm tra lại kết nối mạng
    if (!connectToPortal()) return false; // Kiểm tra lại kết nối tới portal
    
    if (!portal.available())
      portal.print("GetRaspi\n"); // Gửi yêu cầu lấy địa chỉ raspi
    
    byte retry = 20;
    while (portal.connected() && !portal.available() && --retry) delay(100); // đợi 2s để chờ kết quả

    while (portal.available()) { // Nhận kết quả
      String line = portal.readStringUntil('\n');
      if (line.length() > 0 && line[0] != '\n') { // host:port\n
        raspiServer = line.substring(0, line.indexOf(":"));
        raspiPort = line.substring(line.indexOf(":")+1).toInt();
        Serial.printf("[Portal] > Raspi Server: %s\r\n", line.c_str());
        retryAll = 0;
        return true;
      }
    }
  } while (true);
}



/******************************************************
 *                 Station <-> Raspi                  *
 ******************************************************/

void SmartGardenWifiSTA::connectToRaspiServer() {
  while (!raspi.connected()) {
    connectToGarden();  // Đảm bảo kết nối wifi
    if (raspiServer != "" && raspi.connect(raspiServer, raspiPort)) return;
    if (!retrieveRaspiServer()) continue;

    Serial.printf("[Station] > -=> Connecting to Raspi Server: %s:%d", raspiServer.c_str(), raspiPort);
    byte retry = 3;
    while (!raspi.connect(raspiServer, raspiPort)) {
      if (WiFi.status() != WL_CONNECTED) break;  // Kiểm tra kết nối wifi
      Serial.print("."); delay(500);
      if (!--retry) { Serial.println(); raspiServer = ""; break; }
    }
  }
  Serial.printf("\r\n[Station] > Connected to Raspi Server!\r\n");
}

void SmartGardenWifiSTA::raspiServerLoop() {
  // Giữ kết nối liên tục tới server
  if (!raspi.connected()) {
    offLed2
    delay(1000); // Chờ cho raspi kết nối trước
    connectToRaspiServer(); // Trở về vòng lặp chính để kiểm tra lại mạng, IP raspi...
    // Vượt qua được connectToRaspiServer() tức là đã kết nối được tới raspi
    raspi.print(conn.pack(10, conn.defcmd, conn.defcmd, getDevicePackage()));
    onLed2
  }
  
  static String data = "";
  while (raspi.available()) data += raspi.readStringUntil(conn.end) + conn.end;
  if (data.length() > 1) {
    Package pkg;
    Serial.println(data);
    if (conn.unpack(data, pkg)) handleStation(pkg);
    Serial.printf("[%d, %d, %d] %s (rest: %s)\r\n", pkg.cmd, pkg.sub1, pkg.sub2, pkg.msg.c_str(), data.c_str());
  }
  
  #ifdef SENSOR_DHT22 || SENSOR_BH1750 || SENSOR_HC_SR501
  static int timer = millis();
  int now = millis();
  if (now - timer > 5000) {
    if (raspi.connected()) {
      String values = get_station_values();
      Serial.println(String("> Auto send values to server: ") + values);
      raspi.print(conn.pack(12, 1, 1, values));
      timer = millis();
    }
  }
  #endif

  #ifdef SENSOR_HC_SR501
  trigger_detect_movement();
  #endif
}

void SmartGardenWifiSTA::handleStation(Package &pkg) {
  switch (pkg.cmd) {
    case 10:  // Handshake, information exchange
      #ifdef CONTROL
      Serial.printf("[Raspi] > Handshake: %s\r\n", pkg.msg.c_str());
      if (pkg.msg == "{\"state\": true}") digitalWrite(equips[0], 0);
      if (pkg.msg == "{\"state\": false}") digitalWrite(equips[0], 1);
      #endif
      break;
    case 11:  // Raspi send command to Station: sub1 -> equip order, sub2 -> state
      Serial.printf("[Raspi] > Recv command: set %d to %d\r\n", equips[pkg.sub1-1], int(pkg.sub2 == 1));
      if (pkg.sub1 <= sizeof(equips)) digitalWrite(equips[pkg.sub1-1], pkg.sub2 != 1);
      break;

    case 12:  // Read sensor values
      if (raspi.connected()) raspi.print(conn.pack(pkg, get_station_values()));
      break; // Raspi fecth sensor values
  }
}

String SmartGardenWifiSTA::get_station_values() {
  bool hasSensor = false;
  String values = "{";

  #ifdef SENSOR_DHT22
  float temp = dht.readTemperature(), humi = dht.readHumidity();
  if (!isnan(temp) && !isnan(humi)) {
    values += "\"temperature\": " + String(temp) + ", \"humidity\": " + String(humi);
    hasSensor = true;
  }
  #endif

  #ifdef SENSOR_BH1750
  if (hasSensor) values += ",";
  values += "\"light\": " + String(lightMeter.readLightLevel());
  hasSensor = true;
  #endif

  #ifdef SENSOR_HC_SR501
  if (hasSensor) values += ",";
  values += "\"moving\": " + String(analogRead(SENSOR_HC_SR501));
  hasSensor = true;
  #endif

  values += "}";
  return values;
}

#ifdef SENSOR_HC_SR501
void SmartGardenWifiSTA::trigger_detect_movement() {
  static bool prevState = false;
  bool isMove = analogRead(SENSOR_HC_SR501) > 0;
  if (isMove^prevState && raspi.connected()) {
    String pkg = String("{\"moving\": ") + (isMove ? '1' : '0') + "}";
    Serial.println(pkg);
    raspi.print(conn.pack(12,1,1, pkg)); 
    prevState = isMove;
  }
}
#endif

#endif
