
#ifndef _SMART_GARDEN_CONNECTION_H
#define _SMART_GARDEN_CONNECTION_H

#include <ESP8266WiFi.h>
#include <string.h>

/** Package structure
 * 
 *  | START |    Security   | DELM |            Header           | DELM | Data | END_SIGN |
 *  |-------|---------------|------|-----+-----------+-----------|------|------|----------|
 *  | \xfd  | security_info | \xfe | cmd |   sub 1   |   sub 2   | \xfe | Data |   \xff   |
 *  |  1b   |       nb      |      | 1b  |    1b     |    1b     |      |  nb  |    1b    |
 *  [Default CMD is \xf4]                                                                  
 * 
 */

typedef struct _Package {
  public:
  byte cmd, sub1, sub2;
  String msg;
} Package;

class Connection {
  private:
  public:
  const char start = '\xfd';
  const char delimiter = '\xfe';
  const char end = '\xff';
  const char defcmd = '\xf4';

  String pack(Package &pkg);
  String pack(Package &pkg, String msg);
  String pack(byte cmd, byte sub1, byte sub2, String msg);
  bool unpack(String &data, Package &package);

  void test();
};

bool Connection::unpack(String &data, Package &package) {
  // take the first package and return the rest of data
  uint16_t iEnd, iStart, iDelim;

  do { // Loại bỏ các package lỗi
    iEnd = data.indexOf(this->end);
    iStart = data.indexOf(this->start);
    if (iStart > iEnd) data = data.substring(iStart);
  } while (iStart > iEnd);

  if (iStart < iEnd) {
    // START_HEADER_D_DATA_END
    iDelim = data.indexOf(this->delimiter);
    if (iDelim > 0) {
      package.cmd = data.c_str()[1];
      package.sub1 = data.c_str()[2];
      package.sub2 = data.c_str()[3];
      package.msg = data.substring(iDelim+1, data.length() - 1);
      data = data.substring(iEnd + 1, 9999);
      return true;
    }
    data = data.substring(iEnd + 1, 9999);
    return false;
  } else { // iStart == iEnd == -1
    data = "";
    return false;
  }
}

String Connection::pack(Package &pkg) {
  return String(start) + char(delimiter) + char(pkg.cmd) + char(pkg.sub1) + char(pkg.sub2) + char(delimiter) + pkg.msg + char(end);
}
String Connection::pack(Package &pkg, String msg) { // Response to package
  return String(start) + char(delimiter) + char(pkg.cmd) + char(pkg.sub1) + char(pkg.sub2) + char(delimiter) + msg + char(end);
}
String Connection::pack(byte cmd, byte sub1, byte sub2, String msg) {
  return String(start) + char(delimiter) + char(cmd) + char(sub1) + char(sub2) + char(delimiter) + msg + char(end);
}

void Connection::test() {
  Package pkg;

  // START_HEADER_D_DATA_END
  String data = "\xfd\x01\x02\x03\xfeHello This Is My Name!\xff";
  unpack(data, pkg);
  Serial.printf("[Channel] > cmd: %d, sub1: %d, sub2: %d\r\n", pkg.cmd, pkg.sub1, pkg.sub2);
  Serial.printf("[Message] > %s\r\n", pkg.msg.c_str());

  data = pack(1,2,3, "Hello");
  unpack(data, pkg);
  Serial.printf("[Channel] > cmd: %d, sub1: %d, sub2: %d\r\n", pkg.cmd, pkg.sub1, pkg.sub2);
  Serial.printf("[Message] > %s\r\n", pkg.msg.c_str());
  
}


#endif

