
#include "smart_garden_wifi_ap.hpp"

SmartGardenWifiAP centralAP = {};

void SmartGardenWifiAP::setup() {
  setupLed2 then offLed2
  WiFi.mode(WIFI_AP);
  broadcast();    // Khởi động mạng wifi vườn
//  testGoogle();   // Kiểm tra kết nối tới internet
  startPortal();  // Mở cổng thông tin trong vườn
}

void SmartGardenWifiAP::loop() {
  checkAndShowStations();
  portalLoop();
}

/**
 * Phát wifi ra toàn vườn
 */
void SmartGardenWifiAP::broadcast() {
  Serial.printf("[Wifi-AP] > Soft-AP Configuration: %s\r\n", WiFi.softAPConfig(APIP, gateway, subnet) ? "OK!" : "Error!");
  Serial.printf("[Wifi-AP] > Soft-AP Begin: %s\r\n", WiFi.softAP(ssid, password, channel) ? "OK!" : "Error!");
  
  Serial.printf("[Wifi-AP] > Soft-AP IP: %s\r\n", WiFi.softAPIP().toString().c_str());
}

bool SmartGardenWifiAP::testGoogle() {
  Serial.printf("[Wifi-STA] > Connecting to Wall-E: ");
  WiFi.begin("Wall-E", "Wastebasket");
//  WiFi.begin("IUHYRA", "iuhyra@12345");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("."); delay(500);
  }
  Serial.printf("\r\n[Wifi-STA] > Station IP address: %s\r\n", WiFi.localIP().toString().c_str());
  
  static bool couldConnected = false;
  if (!couldConnected) {
    int retry = 0;
    WiFiClient testClient;
    Serial.print("[Test] > Connecting to google (10s test): ");
    while (!testClient.connect("google.com", 80) && retry < 20) {
      Serial.print("."); delay(500);
      retry++;
    }
    if (testClient.connected()) {
      couldConnected = true;
      Serial.println("Connected to Google!");
    } else {
      Serial.println("Cannot connect to Google!");
    }
  }
  return couldConnected;
}

void SmartGardenWifiAP::checkAndShowStations() {
  static byte numClient = 0;
  if (WiFi.softAPgetStationNum() != numClient) {
    numClient = WiFi.softAPgetStationNum();
    Serial.printf("---> Num Clients: %d\r\n", numClient);
//    showStations();
  }
  showStations();
}

void SmartGardenWifiAP::showStations() {
  static String last = "";
  getStationsInfo();
  if (stationsInfo != last) {
    last = stationsInfo;
    Serial.print("----------- Stations ----------\r\n");
    Serial.print(stationsInfo);
    Serial.print("-------------------------------\r\n");
  }
  if (raspiInfo.length() <= 1 && stationsInfo.length() > 0) {
    String buf;
    int iDelim = 0, lastDelim = 0;
    do {
      iDelim = stationsInfo.indexOf('\n', iDelim+1);
      if (iDelim < 0) break;
      buf = stationsInfo.substring(lastDelim, iDelim);
      lastDelim = iDelim + 1;
      if (buf.length() > 0) {
        if (requestRaspiServer(buf)) break;
      } else {
        break;
      }
    } while (true);
  }
  
}

String SmartGardenWifiAP::getStationsInfo() {
  struct station_info *stat_info;
  IPAddress address;
  stationsInfo = "";

  stat_info = wifi_softap_get_station_info();
  while (stat_info != NULL)
  {
    address = stat_info->ip.addr;
    stationsInfo += address.toString() + "\n";
    stat_info = STAILQ_NEXT(stat_info, next);
  }
  return stationsInfo;
}

bool SmartGardenWifiAP::requestRaspiServer(String ip) {
  WiFiClient portal;
  Serial.printf("[Portal] > Check Raspi Portal: %s:%d\r\n", ip.c_str(), raspiPortalPort);
  if (portal.connect(ip, raspiPortalPort)) {
    Serial.printf("[portal] > Found Raspi Portal %s:%d\r\n", ip.c_str(), raspiPortalPort);
    portal.print("raspi_info");
    while (portal.connected()) {
      if (portal.available()) {
        Serial.printf("[portal] > Request raspi info on %s\r\n", ip.c_str());
        String line = portal.readStringUntil('\n');
        if (line.length() > 0) {
          saveRaspiInfo(line);
          return true;
        } else return false;
      }
      delay(10);
    }
  } else {
    Serial.println("[Portal] > Not Raspi Portal!");
  }
  return false;
}
