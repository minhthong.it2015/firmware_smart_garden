

import json

class CloudSyncType1:
  def __init__(self, conn_mgr, gardener):
    self.conn_mgr = conn_mgr
    self.gardener = gardener

    self.conn_mgr.register_request_handle(15, self.request_handle)
    self.conn_mgr.register_request_handle(16, self.request_handle)
    self.conn_mgr.register_request_handle(17, self.request_handle)
    self.conn_mgr.register_request_handle(18, self.request_handle)
    self.conn_mgr.register_request_handle(19, self.request_handle)
    self.conn_mgr.register_request_handle(20, self.request_handle)
    
    self.gardener.equip_mgr.addEventListener("statechange", self.emit_garden_change)

    # self.conn_mgr.socketIOClient.host = "insys-cloud-websocket-server.herokuapp.com"
    self.conn_mgr.socketIOClient.protocol = "ws"

  def request_handle(self, cmd, sub1, sub2, data, client):
    package = None
    if cmd is 15:
      self.gardener.saveGardenID(data)
    elif cmd is 16:
      command = json.loads(data)
      self.gardener.executeCommand(command)
    return self.conn_mgr.build(cmd, sub1, sub2, package)
  
  def emit_garden_change(self, event):
    """ Phát gói tin báo thay đổi trên vườn tới cloud server """
    print("[Sync - Cloud] > emit event: {}".format(event['state']))
    data = {
      "state": event["state"],
      "equipID": event["target"].id
    }
    self.conn_mgr.send2Cloud(self.conn_mgr.build(16, data_2_send=json.dumps(data)))

  def run(self):
    print("[Sync - Cloud] <*> Cloud Synchronize Service is Starting!")
    self.conn_mgr.socketIOClient.add_event_listener("ready", self.onSocketReady)
    if self.conn_mgr.socketIOClient.connected:
      self.onSocketReady()
  
  def onSocketReady(self, event={}):
    self.conn_mgr.socketIOClient.emit("GardenConnect", "ssid", self.onConnectToCloud, True)

  def onConnectToCloud(self, event={}):
    print("[Sync - Cloud] <*> Cloud connection is established!")
    self.conn_mgr.mustSend2Cloud(self.conn_mgr.build(15, data_2_send=self.gardener.gardenID))


  def close(self):
    print("[Sync - Cloud] </> Stop Cloud Sync Services")