# coding=utf-8

from modules.connection.connection_mgr import ConnectionManager

import random
import json
import asyncio

class MobileSync:
  """ Quản lý đồng bộ với các thiết bị mobile. Xử lý trên kênh cmd từ 2-9. """
  def __init__(self, conn_mgr, gardener):
    self.gardener = gardener
    self.conn_mgr = conn_mgr

    # self.conn_mgr.register_request_handle(1, self.request_handle)
    self.conn_mgr.register_request_handle(2, self.request_handle)
    self.conn_mgr.register_request_handle(3, self.request_handle)
    self.conn_mgr.register_request_handle(4, self.request_handle)

    self.security = self.conn_mgr.security

    self.gardener.equip_mgr.addEventListener("statechange", self.emit_garden_change)
  
  @property
  def has_client(self):
    return len(self.conn_mgr.mobile_clients) > 0
  
  def run(self):
    print("[Sync - Mobile] <*> Sync - Mobile Synchronize Service is Ready!")
  
  def emit_garden_change(self, event):
    """ Phát gói tin báo thay đổi trên vườn tới tất cả websocket clients. """
    print("[Sync - Mobile] > emit state: {}".format(event["state"]))
    if self.has_client:
      package = {
        "state": event["state"],
        "equipID": event["target"].id,
        "evaluations": next(iter(event["target"].bound_stations)).evaluations
      }
      self.emit_to_clients(package)
  
  def emit_to_clients(self, package):
    """ Phát gói tin tới tất cả websocket clients. """
    for client in self.conn_mgr.mobile_clients:
      try:
        package = json.dumps(package, ensure_ascii=False)
        asyncio.ensure_future(client.send(self.conn_mgr.build(2,2,1,package)))
      except:
        pass
  
  def request_handle(self, cmd, sub1, sub2, data, client):
    package = None
    if cmd is 2: # cmd 2: [Get Garden State]
      if sub1 is 1: # get info of all the garden
        package = json.dumps(self.gardener.dump(), ensure_ascii=False)
      elif sub1 is 2: # get info of a specific station
        station_id = data
        client.focus_on = station_id
        if sub2 is 1: # get cylinder state (equipments, sensors) (used to realtime update)
          package = json.dumps(self.gardener.get_station_info(station_id), ensure_ascii=False)
        elif sub2 is 2: # get cylinder records for last 6 hours (used to draw chart)
          package = json.dumps(self.gardener.get_records_from(station_id, 6))
    elif cmd is 3: # cmd 3: [Set Garden State]
      # [ station_id, equipment_role, state ]
      station_id, equipment_role, state = json.loads(data.decode('utf-8') if isinstance(data, bytes) else data, encoding='utf-8')
      self.gardener.command_handle(station_id, equipment_role, state)
      package = "YUP"
    elif cmd is 4: # Manage Plant
      if sub1 is 1: # Create new plant
        station_id, plant_type, planting_date, alias = json.loads(data.decode('utf-8') if isinstance(data, bytes) else data, encoding='utf-8')
        self.gardener.planting_new_plant(station_id, plant_type, planting_date, alias)
        package = json.dumps(self.gardener.dump(), ensure_ascii=False)
        # new_plant = Plant(info)
      elif sub1 is 2: # Remove Plant
        station_id, plant_id = json.loads(data.decode('utf-8') if isinstance(data, bytes) else data, encoding='utf-8')
        self.gardener.remove_plant(station_id, plant_id)
        package = json.dumps(self.gardener.dump(), ensure_ascii=False)
    # elif cmd is 4: # get sensors value
    #   if sub1 is 1: # get realtime sensors value
    #     hutemp = self.sensors['hutemp'].value
    #     pH = self.sensors['pH'].value
    #     device_state = "{}|{}|{}|{}".format(time(), hutemp[0], hutemp[1], pH)
    #     print("[BLUESRV] > realtime sensors value: {}".format(device_state), flush=True)
    #     return self.connection.send(client, device_state, cmd, sub1, sub2)
    #   elif sub1 is 2: # get records for last 6 hours
    #     records = self.logger.get_records_last_6h()
    #     sz_records = json.dumps(records)
    #     print("[BLUESRV] > records for last 6 hours ({} records).".format(len(records)), flush=True)
    #     print(sz_records, flush=True)
    #     return self.connection.send(client, sz_records.replace(' ',''), cmd, sub1, sub2)
    #   elif sub1 is 3: # get records since a exactly time
    #     pass
    return self.conn_mgr.build(cmd, sub1, sub2, package)
  

  def close(self):
    print("[Sync - Mobile] </> Stop Sync - Mobile Server")