# -*- coding=utf-8 -*-

from modules.equipment.raspi_gpio_device import RaspiGPIODevice
import modules.helper.ipHelper as ipHelper

import platform
import subprocess
import os
import re
import socket
import ifcfg
from time import sleep, time
import asyncio
import threading
import json



class StationSyncType1:
  def __init__(self, conn_mgr, gardener, garden_AP_signal=['Wall', 'Garden_'], garden_ip_signal=['192.168.244.']):
    self.gardener = gardener
    self.equip_mgr = gardener.equip_mgr
    self.garden_AP_signal = garden_AP_signal
    self.garden_ip_signal = garden_ip_signal
    self.portal = None
    self.portal_port = 80
    self.raspi_portal_port = 4447
    self.LANPort = conn_mgr.LANctl.port
    self.running = False
    self.portal_ok = False

    self.portal_reseter = RaspiGPIODevice({"id": 5, "type": "output"}, reverse=False)

    self.conn_mgr = conn_mgr
    self.conn_mgr.register_request_handle(10, self.request_handle)
    self.conn_mgr.register_request_handle(11, self.request_handle)
    self.conn_mgr.register_request_handle(12, self.request_handle)
    self.conn_mgr.register_request_handle(13, self.request_handle)
    self.conn_mgr.register_request_handle(14, self.request_handle)
  
    self.gardener.equip_mgr.addEventListener("statechange", self.emit_garden_change)

  def _start_conn_keeper_loop(self, loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()

  def run(self):
    self.running = True
    self.ensure_garden_network()
    self.sync_to_portal()



  #############################################################
  #     Đảm bảo kết nối wifi đến mạng của vườn cho Server     #
  #############################################################

  def get_garden_key(self, ssid):
    if 'Wall' in ssid: return "TrashTrash"
    if 'Garden_' in ssid: return "InsysSmartGarden"
    return ""

  @property
  def wpa_supplicant_cfg(self):
    return subprocess.run("sudo cat /etc/wpa_supplicant/wpa_supplicant.conf".split(" "), stdout=subprocess.PIPE).stdout.decode('utf-8')

  @property
  def is_joined(self):
    """ Kiểm tra đã kết nối vào wifi chưa. """
    if not ipHelper.isRaspi(): return True # Tạm thời k xử lý cho windows
    try: return re.match('.*ESSID:".*"', subprocess.run("iwconfig", stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf-8'), re.M|re.I|re.U) != None
    except: return False
  
  def is_know_network(self, ssid, psk):
    """ Kiểm tra đã lưu thông tin mạng wifi nào đó chưa. """
    if re.match('(.|\\s)*(ssid="{}"\\s*.*psk="{}")'.format(ssid, psk), self.wpa_supplicant_cfg, re.M|re.I|re.U):
      return True
    return False

  def save_network(self, ssid, psk):
    """ Lưu thông tin mạng wifi mới. """
    if not self.is_know_network(ssid, psk):
      os.system('wpa_passphrase "{}" "{}" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf >> /dev/null'.format(ssid, psk))
      return True
    else:
      return False
  
  def join_to_network(self):
    """ Kết nối vào mạng wifi """
    os.system('wpa_cli -i wlan0 reconfigure >> /dev/null')

  def scan_for_the_AP(self):
    list_AP = []
    if 'Windows' in platform.system():
      pass
    else:
      rs = subprocess.run("sudo iwlist wlan0 scan".split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      info = rs.stdout.decode('utf-8')
      wifi_list = []
      for m in re.finditer('(Quality=(\d+\/\d+))|(ESSID:\"(.*)\".*?$)', info, re.M|re.I|re.U):
        wifi_list.append(m.groups()[3] if m.groups()[3] else m.groups()[1])

      list_AP = {}
      for wifi, bsid in zip(wifi_list[1::2], wifi_list[::2]):
        list_AP[wifi] = eval(bsid)

      list_AP = sorted(list_AP.items(), key=lambda x: x[1])
      list_AP.reverse()

      return list_AP
  
  def scan_for_the_Garden_Center(self):
    list_AP = self.scan_for_the_AP()
    if not list_AP: return []
    list_Garden = list(filter(lambda network: network[0] and [g for g in self.garden_AP_signal if g in network[0]], list_AP))
    return list_Garden

  def find_and_join_to_Garden_Center(self):
    if self.is_joined:
      print("[Sync - Station] <*> Joined to garden network!")
      return True
    if time() - self.last_joined < 30:
      return False
    list_Garden = self.scan_for_the_Garden_Center()
    if len(list_Garden) > 0:
      print("[Sync - Station] > Garden found: {}".format(list_Garden))
      has_new = False
      for garden in list_Garden:
        has_new = has_new or self.save_network(garden[0], self.get_garden_key(garden[0]))
      if has_new:
        self.join_to_network()
        sleep(2)
      sleep(2)
      if self.is_joined:
        print("[Sync - Station] <*> Joined to garden network!")
        return True
      else:
        print("[Sync - Station] <!> Cannot joining to Garden network!")
        return False

  def _ensure_garden_network(self):
    print("[Sync - Station] <*> Start ensure Garden Network!")
    if not ipHelper.isRaspi(): return
    while self.running:
      if not self.is_joined:
        self.portal_ok = False
        print("[Sync - Station] <!> Out of network!")
        while not self.find_and_join_to_Garden_Center():
          sleep(1)
      else:
        self.last_joined = time()
        sleep(1)
    print("[Sync - Station] <*> Garden Network Ensurance is Stopped!")
  
  def ensure_garden_network(self):
    self.last_joined = 0
    self.ensure_network_thread = threading.Thread(target=self._ensure_garden_network)
    self.ensure_network_thread.start()



  ###########################################################
  #     Quản lý kết nối đồng bộ thông tin giữa các trạm     #
  ###########################################################

  @property
  def has_client(self):
    return len(self.conn_mgr.mobile_clients) > 0

  def emit_garden_change(self, event):
    """ Phát gói tin báo thay đổi trên vườn tới tất cả tcp station. """
    # print("[Sync - Station] > emit event: {}".format(event['reason']))
    # if self.has_client:
    #   package = json.dumps(self.gardener.dump(), ensure_ascii=False)
    #   self.emit_to_clients(package)
    pass
  
  def emit_to_clients(self, package):
    """ Phát gói tin tới tất cả websocket clients. """
    for phone in self.conn_mgr.mobile_clients:
      try:
        asyncio.ensure_future(phone.send(self.conn_mgr.send(None, 10,244,244,package)))
      except:
        pass

  def request_handle(self, cmd, sub1, sub2, data, client):
    package = None
    try:
      if cmd is 10: # Nhận thông tin trạm và gửi trả tình trạng trạm
        client.on_disconnect = lambda: print("[Sync - Station] <!> Station \"{}\" disconnected! ({})".format(client.id, client.addr))
        device_info = json.loads(data.decode('utf-8') if isinstance(data, bytes) else data, encoding='utf-8')
        device_info['socket'] = client
        client.id = device_info["id"] if "id" in device_info else "<Unknow Station>"
        package = self.equip_mgr.load_real_device(device_info)
        # print("[Sync - Station] <*> real device connected: {}".format(device_info['id']))
        package = "OK" if not package else json.dumps(package, ensure_ascii=False)
      elif cmd is 11: # Không nhận, chỉ gửi ở kênh này. Dùng cho các lệnh điều khiển
        pass
      elif cmd is 12: # Nhận dữ liệu cảm biến
        # print("[Sync - Station] <I> values receive: {}".format(data))
        self.equip_mgr.load_values(data, client)

    except Exception as err:
      print("[Sync - Station] <!> Error: {}".format(err))
    
    try:
      package = bytes(package, 'utf-8') if package != None and isinstance(package, str) else None
    except Exception as err:
      print("[Sync - Station] <!> Error: {}".format(err))
    return self.conn_mgr.build(cmd, sub1, sub2, package)



  ###########################################################
  #       Giao tiếp đồng bộ với Portal chung của vườn       #
  ###########################################################

  def get_portal_addr(self, ip_garden):
    if len(ip_garden.split(".")) != 4: return ""
    return ".".join(ip_garden.split(".")[:3] + ['1'])

  def get_raspi_info(self, ip_garden):
    return "raspi:{}:{}\n".format(ip_garden, self.LANPort)

  def send_info_to_portal(self):
    """ Định kỳ gửi thông tin raspi tới Portal. """
    while self.running:
      retry = 0
      while self.running and not self.portal_ok:
        retry += 1
        while self.running: # Cố gắng lấy địa chỉ ip Portal
          ip_garden = ipHelper.ipGarden(self.garden_ip_signal)
          portal_addr = self.get_portal_addr(ip_garden)
          if not portal_addr: sleep(1)
          else: break
        # Cố gắng gửi thông tin tới Portal
        try:
          self.portal = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
          self.portal.connect((portal_addr, self.portal_port))
          self.portal.send(bytes(self.get_raspi_info(ip_garden), encoding="utf-8"))
          # print("[Sync - Station] > Send Raspi Info to Portal: {}".format(self.get_raspi_info(ipv4)))
          try: self.portal_ok = self.portal.recv(100) == b"OK"
          except BaseException as err: pass
          if not self.portal_ok:
            # print("[Sync - Station] > Raspi Info to Portal Failed!")
            pass
          else:
            print("[Sync - Station] > Raspi Info to Portal Succeeded! ({})".format(self.get_raspi_info(ip_garden)))
        except Exception as err:
          print("[Sync - Station] > Connect to Portal error: {}".format(err))
        
      # Nếu thử gửi 5 lần vẫn không được thì reset cứng lại portal
      if self.running and retry >= 3 and not self.portal_ok:
        self.reset_Portal()
        sleep(3) # Đợi 3s để portal khởi động lên
      else:
        sleep(1) # Kiểm tra mỗi 1s để nếu bị đá ra khỏi mạng vườn sẽ gửi lại IP ngay
    print("[Sync - Station] </> Stop sync with Portal")
  
  def open_raspi_portal(self):
    """ Tạo server để Portal request tới các trạm kết nối tìm IP raspi khi portal khởi động sau. """
    self.raspi_portal = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.raspi_portal.bind(("0.0.0.0", self.raspi_portal_port))
    self.raspi_portal.listen()
    print("[Sync - Station] <*> Raspi Portal is listening on {}:{}".format(ipHelper.ipGarden(self.garden_ip_signal), self.raspi_portal_port))
    while self.running:
      try:
        portal, portal_addr = self.raspi_portal.accept()
        req = portal.recv(100)
        if req == b"raspi_info":
          raspi_info = self.get_raspi_info(ipHelper.ipGarden(self.garden_ip_signal))
          print("[Sync - Station] > Portal request Raspi IP: {}".format(raspi_info))
          portal.send(bytes(raspi_info, encoding="utf-8"))
        else:
          portal.close()
      except BaseException as err:
        if isinstance(err, KeyboardInterrupt):
          break

  def sync_to_portal(self):
    self.info_to_portal_thread = threading.Thread(target=self.send_info_to_portal)
    self.info_to_portal_thread.start()
    self.raspi_portal_thread = threading.Thread(target=self.open_raspi_portal)
    self.raspi_portal_thread.start()

  def reset_Portal(self):
    # Đặt chân GPIO nối với chân reset của ESP về 0
    print("[Sync - Station] <!> Reseting ESP Portal!")
    # self.portal_reseter.off()
    # sleep(1)
    # self.portal_reseter.on()
  
  def close(self):
    if self.running:
      self.running = False
      # Cố gắng ngắt socket liên kết với Portal
      try: self.portal.shutdown(socket.SHUT_RDWR)
      except: pass
      try: self.portal.close()
      except: pass

      # Cố gắng ngắt Raspi Portal
      try: self.raspi_portal.shutdown(socket.SHUT_RDWR)
      except: pass
      try: self.raspi_portal.close()
      except: pass
      
      # Cố gắng join các thread
      self.info_to_portal_thread.join()
      self.raspi_portal_thread.join()
      self.ensure_network_thread.join()
    print("[Sync - Station] </> Sync - Stationhronize Service is Stopped!")