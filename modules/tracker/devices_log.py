

import os, datetime
from time import time
import shutil

class DevicesLog:
  def __init__(self, log_dir='./logs'):
    self.log_dir = log_dir
    if not os.path.exists(self.log_dir): os.makedirs(self.log_dir)

  def get_log_file(self, station_id, device_id):
    station_path = "{}/{}".format(self.log_dir, station_id)
    if not os.path.exists(station_path): os.makedirs(station_path)
    return "{}/dev_{}.csv".format(station_path, datetime.datetime.now().strftime('%Y_%m_%d'))

  def check_header(self, devices={}, log_file=''):
    """ Kiểm tra và cập nhập nếu có thay đổi trên header. """
    # Đọc header hiện tại
    headers = ['timestamp']
    from_file = None
    try:
      from_file = open(log_file, "r+")
      header_line = from_file.readline()
      if header_line:
        headers = header_line[:-1].split(',')
    except:
      pass
    
    # Kiểm tra thay đổi
    is_change = False
    for key in devices:
      if key and key not in headers:
        headers.append(key)
        is_change = True

    # Nếu có thay đổi thì cập nhập lại header trên file
    if is_change:
      header_line = ','.join(headers) + '\n'
      to_file = open(log_file, mode="w")
      to_file.write(header_line)
      if from_file:
        shutil.copyfileobj(from_file, to_file)
      to_file.close()
    if from_file:
      from_file.close()
    return headers
  
  def build_records(self, devices, log_file):
    """ Trả về mảng các thông số môi trường theo đúng thứ tự trong header. """
    headers = self.check_header(devices, log_file)
    records = []
    for head in headers[1:]:
      records.append(str(devices[head]) if head in devices else '')
    return records
  
  def log(self, station_id, device_id, devices={}):
    """ Lưu điều kiện môi trường trên một trạm xác định từ một device xác định.
    ---
    [Định dạng]
    - ``timestamp``,``a``,``b``,``c``,``d``\n
    """
    log_file = self.get_log_file(station_id, device_id)
    records = self.build_records(devices, log_file)

    with open(log_file, "a+") as fp:
      fp.write('{},{}\n'.format(round(time()), ','.join(records)))
      fp.close()