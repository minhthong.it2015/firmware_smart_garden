
""" Module Observer hỗ trợ theo dõi sự thay đổi trên các file để phát ra sự kiện báo thay đổi cập nhập lên phía trên ứng dụng.

[Một số file được theo dõi]
- ``plant_lib.json``: Thông tin chăm sóc cây trồng
  + 
- ``stations.json``: Thông tin thiết bị trên các trạm
  + Khi có trạm mới thì biết để điều khiển trạm đó
  + Khi có thiết bị thay đổi thì biết để điều khiển hay nhận dữ liệu từ thiết bị mới
"""

import os.path as path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import asyncio

class _FileModifiedTracker(FileSystemEventHandler):
  def setup(self, file_path='', callback=None):
    self.file_path = file_path
    self.callback = callback

  def on_modified(self, event):
    try:
      
      if path.samefile(event.src_path, self.file_path): self.callback(event)
    except: pass

  def on_created(self, event):
    self.on_modified(event)


class FileModifiedTracker(FileSystemEventHandler):
  def track(self, file_path, callback=None, loop=None):
    self.file_path = file_path
    self.dir = path.dirname(file_path)
    self.loop = loop if loop else asyncio.get_event_loop()
    self.none_event_loop = asyncio.new_event_loop()

    self.tracker = _FileModifiedTracker()
    self.tracker.setup(file_path, self._on_event)

    self.listeners = []
    if callback: self.listeners.append(callback)

    self.observer = Observer()
    self.observer.schedule(self.tracker, self.dir, recursive=False)
    self.observer.start()
  
  def _on_event(self, event):
    asyncio.set_event_loop(self.loop)
    for listener in self.listeners:
      listener(event)
    asyncio.set_event_loop(self.none_event_loop)