

class Listenable:
  def __init__(self, avaiEvent=[]):
    self._listeners = {}
    try:
      for evt in avaiEvent:
        self._listeners[evt] = []
    except:
      pass
  
  @property
  def availableEvent(self):
    return list(self._listeners.keys())
  
  def addEventListener(self, event, callback):
    """ Đặt lắng nghe trên sự kiện """
    if event not in self._listeners:
      self._listeners[event] = []
    self._listeners[event].append(callback)
    return True
  
  def removeEventListener(self, event, callback=None):
    """ Gỡ bỏ lắng nghe trên sự kiện """
    if event in self._listeners and callback in self._listeners[event]:
      self._listeners[event].remove(callback)
      return True
    else:
      return False
  
  def removeAllEventListener(self):
    """ Gỡ bỏ toàn bộ lắng nghe """
    self._listeners = {}
    return True

  def dispatchEvent(self, event, info):
    """ Phát tán sự kiện """
    if event in self._listeners:
      info["type"] = event
      if event is "stationschange":
        print("???")
      for callback in self._listeners[event]:
        try:
          callback(info)
        except BaseException as err:
          print("[Listenable] <!> {}/{} - Error: {}".format(event, info, err))