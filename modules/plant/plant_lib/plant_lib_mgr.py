

# coding=utf-8

"""
## Quản lý thư viện chăm sóc cây trồng
+ 1 ``ListPlant`` có nhiều ``Plant``
+ 1 ``Plant`` có nhiều ``GrowthStage``
+ 1 ``GrowthStage`` có nhiều ``WaterPoints``
+ 1 ``WaterPoints`` có nhiều ``WaterTime``
____________________________________

`Cho giai đoạn hiện tại:
 - Mỗi bộ xử lý trung tâm sẽ quản lý nhiều trụ.
 - Mỗi trụ sẽ có pin khác nhau (các pin được cấp tự động).
   + pin máy bơm nước.
   + pin van dinh dưỡng.
 - Trên mỗi trụ sẽ có thêm các đèn báo hiệu riêng.
   + đèn báo tình trạng hoạt động.
   + đèn báo tình trạng môi trường.
   + đèn báo chế độ hoạt động
 - Dành riêng 5 GPIO cho đèn báo tại bộ xử lý trung tâm (bảng mạch chính).
`
"""

from modules.plant.models.plant_lib import PlantLib
from modules.plant.models.growth_stage import GrowthStage
from modules.plant.models.environment_factors import ENV_TYPE_MAPPING
from modules.plant.models.living_environment import LivingEnviroment
from modules.tracker.observer import FileModifiedTracker

import json
import os


class PlantLibraryManager:
  """ Module quản lý thư viện chăm sóc cây trồng
  """
  def __init__(self, plant_lib_path='./assets/plants_lib.json'):
    self.plant_lib_list = []  # Mảng lưu danh sách PlantLib trong thư viện
    self._event_listeners = {} # Mảng lưu các listener
    self._event_listeners["change"] = [] # Được lắng nghe từ UserPlantManager
    self.timestamp = 0 # Thời gian cập nhập cuối của plantlib
    if plant_lib_path != None:
      self.plant_lib_path = plant_lib_path
      self.library = self.load_library(self.plant_lib_path)
      self.plant_lib_list = self.parse_library(self.library)
    self.tracking_changes()
  
  def load_library(self, plant_lib_path=None):
    if plant_lib_path == None:
      plant_lib_path = self.plant_lib_path
    else:
      self.plant_lib_path = plant_lib_path

    try:  # Nạp file library
      with open(plant_lib_path, 'r', encoding='utf-8') as fp:
        library = json.load(fp, encoding="utf-8")
      print("[System - Plant Library] <*> Plant Library loaded ({} plants).".format(len(library)))
    except Exception as err:
      print("[System - Plant Library] <!> Failed to load Plant Library file!")
      print("[System - Plant Library] > {}".format(err))
      library = None
    
    return library

  def parse_library(self, library):
    """ Chuyển json object sang class object """
    plant_lib_list = []
    if library != None:
      try:  # Xử lý file library đưa thông tin vào chương trình
        for plant in self.library:
          plant_lib_list.append(PlantLibraryManager.parse_plant_lib(plant))
        print("[System - Plant Library] <*> Plant Library parsed ({} plants).".format(len(plant_lib_list)))
      except Exception as err:
        print("[System - Plant Library] <!> Failed to parsed Plant Library!")
        print("[System - Plant Library] > {}".format(err))
    return plant_lib_list

  @staticmethod
  def parse_plant_lib(plant_lib):
    plant_lib_name = plant_lib['plant_name'] if 'plant_name' in plant_lib else 'unknow'
    plant_lib_type = plant_lib['plant_type'] if 'plant_type' in plant_lib else 'unknow'
    growth_stages = []
    if 'growth_stages' in plant_lib:
      for stage in plant_lib['growth_stages']:
        living_environments = []
        if 'living_environments' in stage:
          for environments in stage['living_environments']:
            usefor = environments['usefor'] if 'usefor' in environments else 'all'
            env_factors = []
            for env in environments['environments']:
              if env['name'] in ENV_TYPE_MAPPING:
                env_factor = ENV_TYPE_MAPPING[env['name']](env)
                env_factors.append(env_factor)
              else:
                print("[System - Plant Library] <!> Unrecognize Environment Factor: {}".format(env['name']))
            living_environments.append(LivingEnviroment(env_factors, usefor))
          growth_stages.append(GrowthStage(stage['stage_order'], stage['stage_name'], (stage['start'], stage['end']), living_environments))
    return PlantLib(plant_lib_name, plant_lib_type, growth_stages)
  
  def tracking_changes(self):
    self.tracker = FileModifiedTracker()
    self.tracker.track(self.plant_lib_path, self._on_plantlib_changes)
  
  def _on_plantlib_changes(self, event):
    print("[System - Plant Library] <~> Plant Library changes!")
    new_library = self.load_library(event.src_path)
    new_plant_lib_list = self.parse_library(new_library)
    try:
      has_new = 0
      new_list = []
      for new_plant_lib in new_plant_lib_list:
        old_plant_libs = list(filter(lambda plant_lib: plant_lib.plant_type == new_plant_lib.plant_type, self.plant_lib_list))
        if old_plant_libs:
          for plant_lib in old_plant_libs: plant_lib.update(new_plant_lib)
        else:
          new_list.append(new_plant_lib)
          has_new += 1
      self.library = new_library
      self.plant_lib_list.extend(new_list)
      print("[System - Plant Library] > Updated Plant Library! (+{} plants)".format(has_new))
      self.emit_event("change", {"target": self, "list": self.plant_lib_list, "new": new_list})
    except Exception as err:
      print("[System - Plant Library] <!> Failed to Update Plant Library: {}".format(err))


  def add_event_listener(self, callback, event=""):
    """ event = "change" """
    if not event:
      for event in self._event_listeners:
        self._event_listeners[event].append(callback)
    else:
      if event not in self._event_listeners:
        return False
      self._event_listeners[event].append(callback)
    return True
  
  def remove_event_listener(self, callback, event=""):
    if event != "":
      if event in self._event_listeners and callback in self._event_listeners[event]:
        event.remove(callback)
        return True
      else:
        return False
    else:
      success = False
      for event in self._event_listeners:
        if callback in event:
          event.remove(callback)
          success = True
      return success
  
  def emit_event(self, event_type, event_obj):
    for listener in self._event_listeners[event_type]:
      listener(event_obj)

  def get_plant_by_type(self, plant_type):
    """ Lấy con trỏ tới thông tin chăm sóc cây trong thư viện """
    for plant_lib in self.plant_lib_list:
      if plant_lib.plant_type == plant_type:
        return plant_lib
    return None
  
  def dump(self):
    pass
