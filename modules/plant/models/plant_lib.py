# coding=utf-8

import datetime

class PlantLib:
  """ Thông tin đầy đủ về cây trồng ở dạng đối tượng. Dữ liệu được nạp từ Plant Library. """
  def __init__(self, plant_name='', plant_type='', growth_stages=None):
    self.plant_name = plant_name
    self.plant_type = plant_type
    self.growth_stages = growth_stages if isinstance(growth_stages, list) else []
  
  def get_current_growth_stage(self, planting_date):
    start = planting_date
    now = datetime.datetime.now()
    daypass = now - start
    for stage in self.growth_stages:
      if stage.start <= daypass.days + 1 <= stage.end:
        return stage
    return self.growth_stages[-1] if len(self.growth_stages) > 0 else None

  def get_harvest_time(self):
    return self.growth_stages[-1].end if len(self.growth_stages) > 0 else 0

  def update(self, new_plant_lib):
    self.plant_name = new_plant_lib.plant_name
    self.growth_stages = new_plant_lib.growth_stages