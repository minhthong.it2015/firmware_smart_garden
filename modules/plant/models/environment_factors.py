# coding=utf-8

from modules.timing.time_action import TimeActionGroup
from modules.tracker.logger import Logger

class IEnvironmentFactor:
  """ Điều kiện môi trường tiêu chuẩn của cây trồng.
  Được gắn liền với ``PlantLib > GrowthStage > LivingEnviroment``. """
  def __init__(self, name, info_in_lib=None):
    self.name = name
    self.info = info_in_lib
    # self.parse_from_lib(info_in_lib)

  def parse_from_lib(self, info_in_lib):
    raise NotImplementedError

  def check(self, envs, planting_date):
    """ Kiểm tra thông số môi trường có hợp lệ và đưa ra hành động cần thiết để điều chỉnh.
     - ``envs``: Đối tượng dạng dict lưu các thông số môi trường tại trạm.
     - ``planting_date``: Ngày bắt đầu trồng cây (datetime object).
    """
    problems = []
    env_factor = envs['env_factor'] if 'env_factor' in envs else None
    if not env_factor: return problems
    raise NotImplementedError



###################################################
#              Environment Factors                #
###################################################

class WaterCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('Water')
    self.parse_from_lib(info_in_lib)

  def parse_from_lib(self, info_in_lib):
    self.actions_on = info_in_lib['actions_on'] if 'actions_on' in info_in_lib else []
    self.actions_on = list(map(lambda action: {"action": action, "reasons": "water_timing"}, self.actions_on))
    self.actions_off = info_in_lib['actions_off'] if 'actions_off' in info_in_lib else []
    self.actions_off = list(map(lambda action: {"action": action, "reasons": "water_timing"}, self.actions_off))
    self.water_points = TimeActionGroup()
    for con in info_in_lib['water']:
      self.water_points.append_time(con)

  def check(self, envs, planting_date):
    problems = []
    check_result = self.water_points.check_time(planting_date)
    if check_result:
      problems.append({"problem": "water_timing", "detail": check_result, "actions": self.actions_on})
    else:
      problems.append({"problem": "water_timing", "detail": check_result, "actions": self.actions_off})
    return problems

class TemperatureCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('Temperature')
    self.parse_from_lib(info_in_lib)
  
  def parse_from_lib(self, info_in_lib):
    self.min_temp = info_in_lib['temperature'][0]
    self.max_temp = info_in_lib['temperature'][1]
    self.offset = info_in_lib['temperature'][2]
    self.subthreshold = info_in_lib['subthreshold'] if 'subthreshold' in info_in_lib else []
    self.superthreshold = info_in_lib['superthreshold'] if 'superthreshold' in info_in_lib else []
    self.inrange = info_in_lib['inrange'] if 'inrange' in info_in_lib else []
    self.saferange = info_in_lib['saferange'] if 'saferange' in info_in_lib else []
  
  def check(self, envs, planting_date):
    """ Kiểm tra xem có vấn đề gì không và trả về kết quả có bao gồm "actions" """
    problems = []
    env_temp = envs['temperature'] if 'temperature' in envs else None
    if not env_temp: return problems

    if env_temp > self.max_temp + self.offset:
      problems.append({"problem": "high_temp", "detail": {"type": "value_bound", "min": self.min_temp, "max": self.max_temp, "offset": self.offset, "temp": env_temp}, "actions": self.superthreshold})
    elif env_temp < self.min_temp - self.offset:
      problems.append({"problem": "low_temp", "detail": {"type": "value_bound", "min": self.min_temp, "max": self.max_temp, "offset": self.offset, "temp": env_temp}, "actions": self.subthreshold})
    elif env_temp >= self.min_temp and env_temp <= self.max_temp:
      problems.append({"problem": "best_temp", "detail": {"type": "value_bound", "min": self.min_temp, "max": self.max_temp, "offset": self.offset, "temp": env_temp}, "actions": self.saferange})
    else:
      problems.append({"problem": "good_temp", "detail": {"type": "value_bound", "min": self.min_temp, "max": self.max_temp, "offset": self.offset, "temp": env_temp}, "actions": self.inrange})
    return problems

class MistCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('Water')
    self.parse_from_lib(info_in_lib)

  def parse_from_lib(self, info_in_lib):
    self.actions_on = info_in_lib['actions_on'] if 'actions_on' in info_in_lib else []
    self.actions_on = list(map(lambda action: {"action": action, "reasons": "misting_timing"}, self.actions_on))
    self.actions_off = info_in_lib['actions_off'] if 'actions_off' in info_in_lib else []
    self.actions_off = list(map(lambda action: {"action": action, "reasons": "misting_timing"}, self.actions_off))
    self.misting_points = TimeActionGroup()
    for con in info_in_lib['misting']:
      self.misting_points.append_time(con)

  def check(self, envs, planting_date):
    problems = []
    check_result = self.misting_points.check_time(planting_date)
    if check_result:
      problems.append({"problem": "misting_timing", "detail": check_result, "actions": self.actions_on})
    else:
      problems.append({"problem": "misting_timing", "detail": check_result, "actions": self.actions_off})
    return problems

class HumidityCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('Humidity')

class LightCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('Light')
    self.parse_from_lib(info_in_lib)

  def parse_from_lib(self, info_in_lib):
    self.actions = info_in_lib['actions'] if 'actions' in info_in_lib else []

    self.light_points = TimeActionGroup()
    self.max_lux = self.min_lux = self.offset = None
    for con in info_in_lib['light']:
      if 'lux_range' in con:
        self.min_lux = con['lux_range'][0]
        self.max_lux = con['lux_range'][1]
        self.offset = con['lux_range'][2]
        self.subthreshold = info_in_lib['subthreshold'] if 'subthreshold' in info_in_lib else []
        self.superthreshold = info_in_lib['superthreshold'] if 'superthreshold' in info_in_lib else []
        self.inrange = info_in_lib['inrange'] if 'inrange' in info_in_lib else []
      elif TimeActionGroup.are_you(con):
        self.light_points.append_time(con)
  
  def check(self, envs, planting_date):
    problems = []
    env_lux = envs['light'] if 'light' in envs else None
    if not env_lux: return problems

    # if self.max_lux is not None:
    #   if env_lux > self.max_lux + self.offset or env_lux < self.min_lux - self.offset:
    #     if self.equipment_set.light.start('light'):
    #       print("[EnvFactor] > Start lighting by light: {} lux out of [{}-{}]±{} lux ({})".format(env_lux, self.min_lux, self.max_lux, self.offset, Logger.time()))
    #   else:
    #     if self.equipment_set.light.stop('light'):
    #       print("[EnvFactor] > Stop lighting by light: {} lux in range [{}-{}]±{} lux ({})".format(env_lux, self.min_lux, self.max_lux, self.offset, Logger.time()))

    check_result = self.light_points.check_time(planting_date)
    if check_result:
      problems.append({"problem": "light_timing", "detail": check_result})
    return problems

class RotateCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('Rotate')
    self.parse_from_lib(info_in_lib)

  def parse_from_lib(self, info_in_lib):
    self.rotate_points = TimeActionGroup()
    for con in info_in_lib['rotate']:
      self.rotate_points.append_time(con)

  def check(self, envs, planting_date):
    problems = []
    check_result = self.rotate_points.check_time(planting_date)
    if check_result:
      problems.append(check_result)
    return problems

class PPMCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('ppm')

class pHCondition(IEnvironmentFactor):
  def __init__(self, info_in_lib):
    super().__init__('pH')


ENV_TYPE_MAPPING = {
  "water": WaterCondition,
  "temperature": TemperatureCondition,
  "mist": MistCondition,
  "humidity": HumidityCondition,
  "light": LightCondition,
  "rotate": RotateCondition,
  "ppm": PPMCondition,
  "pH": pHCondition
}