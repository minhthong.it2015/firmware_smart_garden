# -*- coding: utf-8 -*-

"""
## Phân lớp module:
+ ``PlantList`` quản lý các ``Plant``
+ ``Plant`` lưu thông tin cây trồng
____________________________________

`Cho giai đoạn hiện tại:
 - Mỗi bộ xử lý trung tâm sẽ quản lý nhiều trụ.
 - Mỗi trụ sẽ có pin khác nhau (các pin được cấp tự động).
   + pin máy bơm nước.
   + pin van dinh dưỡng.
 - Trên mỗi trụ sẽ có thêm các đèn báo hiệu riêng.
   + đèn báo tình trạng hoạt động.
   + đèn báo tình trạng môi trường.
   + đèn báo 
 - Dành riêng 5 GPIO cho đèn báo tại bộ xử lý trung tâm (bảng mạch chính).
`

## Mô tả các module:
**PlantList** - Quản lý danh sách cây trồng
  * 

**Plant** - Quản lý thông tin cơ bản cây trồng
  * Tên cây trồng
  * Các giai đoạn phát triển
"""

import datetime
import json


class UserPlant:
  """ Thông tin cây trồng. Được khởi tạo từ UserPlantManager.
  - Có 2 cách khởi tạo:
    + Cách 1: Nạp vào từ đối tượng dạng dict, được truyền qua tham số @json.
    + Cách 2: Truyền từng thông tin vào theo các tham số.
  """
  def __init__(self, alias='', plant_type='', planting_date='', plant_id='', location='',\
                plant_lib=None, plant_lib_mgr=None, json=None):
    """ ``planting_date``: d/m/yyyy """
    if json != None:
      try:
        plant_id = json['id']
        alias = json['alias']
        plant_type = json['plant_type']
        planting_date = json['planting_date']
        location = json['location'] if 'location' in json else ''
      except Exception as err:
        print("[UserPlant] > Load user plant error: {}".format(err))
    
    if planting_date != '':
      day, month, year = planting_date.split('/')
      self.planting_date_obj = datetime.datetime(day=int(day), month=int(month), year=int(year))
    else:
      self.planting_date_obj = None

    self.alias = alias
    self.plant_type = plant_type
    self.id = plant_id
    self.location = location
    
    self.plant_lib = None
    self.attach_plant_lib(plant_lib=plant_lib, plant_lib_mgr=plant_lib_mgr)

  def attach_plant_lib(self, plant_lib=None, plant_lib_mgr=None):
    if plant_lib_mgr != None:
      self.plant_lib = plant_lib_mgr.get_plant_by_type(self.plant_type)
    if plant_lib != None:
      self.plant_lib = plant_lib
  
  def update(self, new_plant):
    self.alias = new_plant.alias
    self.plant_type = new_plant.plant_type
    self.location = new_plant.location
    self.planting_date_obj = new_plant.planting_date_obj

  @property
  def planting_date(self):
    """ Ngày bắt đầu ươm mầm (datetime object). """
    return self.planting_date_obj

  @property
  def planting_date_str(self):
    """ Ngày bắt đầu ươm mầm (string). """
    return self.planting_date.strftime('%d/%m/%Y') if self.planting_date else ""
  
  @property
  def harvest_time(self):
    """ Thời gian đến ngày thu hoạch. """
    return self.plant_lib.get_harvest_time() if self.plant_lib else 0

  @property
  def current_living_environments(self):
    """ Trả về mảng các EnvironmentFactor lưu trữ thông tin điều kiện môi trường tối ưu cho cây trồng. """
    cur_gr_stage = self.current_growth_stage
    return cur_gr_stage.living_environments if cur_gr_stage else []

  @property
  def current_growth_stage(self):
    return self.plant_lib.get_current_growth_stage(self.planting_date) if self.plant_lib else None

  def check_living_environment(self, env_condictions, env_type):
    """ Kiểm tra điều kiện môi trường thật hiện tại với điều kiện môi trường tối ưu xem có cần điều chỉnh gì không. Nếu có trả về một đối tượng dạng dict lưu trữ yêu cầu thay đổi điều kiện môi trường như bật/tắt bơm, phun sương làm mát, bật đèn chiếu sáng... """
    problems = []
    if self.plant_lib:
      # Tìm thông tin môi trường sống cho trạm có kiểu môi trường env_type (list)
      living_environments = self.current_living_environments
      if 'all' in env_type:
        find = living_environments[0] if len(living_environments) > 0 else None
      else:
        find = [living_env for living_env in living_environments if [u4 for u4 in living_env.usefor if u4 in env_type]]
        find = find[0] if len(find) > 0 else None

      # Nếu tìm thấy thì kiểm tra môi trường đảm bảo không
      if find:
        for env_factor in find.env_factors:
          problems += env_factor.check(env_condictions, self.planting_date)
    return problems
  
  def dump(self):
    user_plant = {
      "alias": self.alias,
      "type": self.plant_type,
      "id": self.id,
      "planting_date": self.planting_date_str,
      "harvest_time": self.harvest_time,
      "location": self.location
    }
    return user_plant
