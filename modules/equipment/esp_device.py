
from modules.equipment.real_device import IRealDevice
import asyncio
import json
import threading
import random
from time import sleep

import datetime

class ESPDevice(IRealDevice):
  def __init__(self, esp_info, emulate_control=False, emulate_sensors=False):
    super().__init__(esp_info)
    self._state = {}
    self.socket = esp_info['socket'] if 'socket' in esp_info else None

    self.emulate_control = emulate_control
    self.emulate_sensors = emulate_sensors
    if 'emulate' in esp_info:
      self.emulate_control = 'control' in esp_info['emulate'] or self.emulate_control
      self.emulate_sensors = 'sensors' in esp_info['emulate'] or self.emulate_sensors

    self.emulate_thread = None
    if self.emulate_control or self.emulate_sensors:
      loop = asyncio.get_event_loop()
      self.emulate_thread = asyncio.run_coroutine_threadsafe(self.emulation(), loop)
  
  @property
  def emulate(self):
    return self.emulate_control or self.emulate_sensors
  
  async def emulation(self):
    while self.emulate_control or self.emulate_sensors:
      await asyncio.sleep(random.randint(5, 10))
      # if self.emulate_control:
      #   self.state = random.randint(1,100) < 30
      if self.emulate_sensors:
        h = datetime.datetime.now().hour
        state = {
          "temperature" : self.calcTemp(h) + random.randrange(-30, 30, 1)/10,
          "humidity" : self.calcHumi(h),
          "light" : self.calcLight(h) + random.randint(0, 500),
          "pH" : random.randint(30,90)/10,
          "ppm": random.randint(100, 2000)
        }
        self.update_state(json.dumps(state))
  
  @staticmethod
  def calcLight(h):
    if (h >= 18 or h <= 4): return 0
    if (h <= 9): return 1000
    if (h >= 12 and h <= 14): return 2500
    if (h <= 15): return 2000
    return 1000
  
  @staticmethod
  def calcTemp(h):
    if (h >= 18 or h <= 4): return 20
    if (h <= 9): return 25
    if (h >= 12 or h <= 14): return 29
    if (h <= 15): return 27
    return 26
  
  @staticmethod
  def calcHumi(h):
    return random.randint(70, 90)

  ############################################

  def attach_virtual_device(self, virtual_device):
    super().attach_virtual_device(virtual_device)
    self.set_state(virtual_device.state)

  @property
  def connected(self):
    return self.socket != None

  def attach_socket(self, socket):
    """ Gắn kết với socket của esp. """
    if socket:
      self.socket = socket
  
  def update(self, real_device):
    """ Cập nhập thông tin thiết bị (Từ update file hay từ sự thay đổi socket) """
    if real_device.socket:
      self.socket = real_device.socket

  def are_you(self, signal):
    """ Kiểm tra ESPDevice này có phải của socket nào đó khác không """
    try:
      return self.socket.addr == signal.addr
    except:
      return False

  ############################################

  @property
  def state(self):
    return self._state

  @state.setter
  def state(self, state):
    self._state = state
  
  def get_state_json(self):
    return json.dumps(self.state, ensure_ascii=False)

  def set_state(self, state):
    """ Đặt trạng thái từ trên xuống """
    self.state = state
    if self.socket:
      try:
        self.socket.send(11, 1, self.get_state_json()) # Nhánh 11/1
      except BaseException as err:
        print("[Device - ESP] > Send state failed: {}".format(err))
        self.socket = None

  def update_state(self, szState):
    """ Cập nhập trạng thái từ thiết bị thật gửi lên """
    try:
      self.state = json.loads(szState.decode('utf-8') if isinstance(szState, bytes) else szState, encoding="utf-8")
      self.virtual_device.update_state(self.state) # Cập nhập trạng thái lên trên
    except Exception as err:
      print("[Device - ESP] > Parsing error: {}".format(err))

  def request_update_state(self):
    """ Nhận được yêu cầu làm mới lại dữ liệu cảm biến (từ server gửi xuống trạm). """
    pass

  ############################################

  def detach(self):
    super().detach()
    self.emulate_control = self.emulate_sensors = False
    if self.emulate_thread:
      try: self.emulate_thread.cancel()
      except: pass
    if self.connected:
      try: self.socket.close()
      except: pass