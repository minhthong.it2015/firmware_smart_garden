

# Bảng liệt kê các loại cảm biến

 - `Light`: Ánh sáng
 - `Temperature`: Nhiệt độ
 - `Humidity`: Độ ẩm
 - `EC`, `PPM`: Nồng độ chất rắn hòa tan
 - `pH`: Độ pH