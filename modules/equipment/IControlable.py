

class ISwitch:
  def __init__(self):
    self._reasons = []
    self._last_reason = ""

  def on(self, reasons=''):
    self._last_reason = reasons
    if isinstance(reasons, list):
      for reason in reasons:
        if reason not in self._reasons:
          self._reasons.append(reason)
    elif reasons not in self._reasons:
      self._reasons.append(reasons)
    self.state = True

  def off(self, reasons=''):
    """ Tắt thiết bị. Đặt reasons về "all_reasons" sẽ gỡ toàn bộ reason. """
    self._last_reason = reasons
    if isinstance(reasons, list):
      for reason in reasons:
        if reason in self._reasons:
          self._reasons.remove(reason)
    elif reasons in self._reasons:
      self._reasons.remove(reasons)
    if reasons == 'all_reasons' or reasons == 'user_set':
      self._reasons = []
    if len(self._reasons) == 0:
      self.state = False
  
  def toggle(self, reasons=''):
    if self.state: self.off(reasons)
    else: self.on(reasons)