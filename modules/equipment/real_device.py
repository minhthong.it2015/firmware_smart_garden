



class IRealDevice:
  """ Interface của thiết bị thật, cầu nối giữa thiết bị ảo và thiết bị thật """
  def __init__(self, device_info):
    self._virtual_device = None
    self.id = device_info['id']
    self.device_info = device_info
  
  @property
  def connected(self):
    """ Phương thức kiểm tra đã kết nối với thiết bị thật chưa"""
    raise NotImplementedError
  
  @property
  def emulate(self):
    return False

  def attach_virtual_device(self, virtual_device):
    """ Phương thức thực hiện lúc gắn kết với thiết bị ảo, cả 2 bên thật ảo sẽ đều nắm giữ định danh của nhau. Và gắn kết theo kiểu "1 Ảo - Nhiều Thật" """
    self.virtual_device = virtual_device
  
  def detach_virtual_device(self):
    """ Gỡ gắng kết với thiết bị ảo """
    self.virtual_device = None

  def put_state(self, name, state):
    """ Đặt trạng thái con mới cho thiết bị thật """
    raise NotImplementedError

  def set_state(self, state):
    """ Đặt trạng thái mới cho thiết bị thật """
    raise NotImplementedError

  def update_state(self, state):
    """ Cập nhập trạng thái từ dưới lên -> thiết bị ảo -> trạm """
    raise NotImplementedError

  def are_you(self, signal):
    """ Kiểm tra một liên kết nào đó có thuộc thiết bị thật này không. Dùng để kết nối lại sau khi lỗi. """
    raise NotImplementedError
  
  def detach(self):
    """ Ngắt thiết bị hoặc ngưng giả lập. """
    self.detach_virtual_device()