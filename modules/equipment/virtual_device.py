
from modules.tracker.listenable import Listenable

class VirtualDevice(Listenable):
  """
  **VirtualDevice + RealDevice**
  - VirtualDevice và RealDevice sẽ như 2 đầu cầu kết nối giữa thiết bị
  được khai báo trên trạm và thiết bị thật.
  - RealDevice sẽ gắn kết với liên kết tcp/rs485/uart... đến thiết bị vật lý
  - VirtualDevice sẽ hoạt động như thiết bị trên hệ thống
  - Khi muốn bật tắt thiết bị thật sẽ gửi lệnh tới thiết bị ảo, từ thiết bị ảo
  sẽ gửi lệnh xuống tất cả thiết bị thật được gắn kết để thực hiện lệnh.
  - Với thiết bị là cảm biến sẽ gửi dữ liệu về ngay khi có thay đổi. Ở trên
  thiết bị ảo có thể đăng ký sự kiện để xử lý khi có thay đổi.
  - Hỗ trợ gửi yêu cầu làm mới lại dữ liệu cảm biến tới thiết bị vật lý.
  ------------------------------
  --- ``device_info['type']``: ``<conn_type>_<device_type>``
  ------------------------------
  ``device_type``: Loại thiết bị
  - sensors: loại cảm biến (Dữ liệu gửi về sẽ được phân tích json để hiểu)
  - control: loại bật tắt
  ------------------------------
  ``conn_type``: Loại kết nối
  - esp: Kết nối qua mạng wifi LAN của module esp (dùng tên esp cho quen thuộc)
  - rs485: Kết nối qua module rs485
  - gpio: Kết nối trực tiếp qua chân GPIO của raspi
  """
  def __init__(self, device_info, bound_station=None):
    Listenable.__init__(self)

    self._state = {}
    self._real_devices = []
    self._reasons = []
    self._last_reason = ''

    self.name = device_info['name']
    self.id = device_info['id']
    self.device_info = device_info
    self.bound_stations = set()
    if bound_station:
      self.bound_stations.add(bound_station)
    # self.bound_stations.difference

    self.roles = device_info['roles'] if 'roles' in device_info else []
    self._load_default_state_by_role()
    if 'state' in device_info:
      self.state.update(device_info['state'])
  
  def bound_to_station(self, station):
    return self.bound_stations.add(station)
  
  def _load_default_state_by_role(self):
    for role in self.roles:
      if role != "sensors":
        self.state[role] = False

  def update(self, new_virtual_device):
    """ Cập nhập thông tin thiết bị (name, roles, bound_station)"""
    self.roles = new_virtual_device.roles
    self.device_info = new_virtual_device.device_info
    # self.bound_stations = new_virtual_device.bound_stations
  
  def attach_real_device(self, real_device):
    if not [dev for dev in self._real_devices if dev.id == real_device.id]:
      self._real_devices.append(real_device)
      real_device.attach_virtual_device(self)

  def detach_real_device(self, device):
    if device in self._real_devices:
      self._real_devices.remove(device)

  @property
  def state(self):
    return self._state
  
  @state.setter
  def state(self, state):
    if state and isinstance(state, dict):
      self.state.update(state)
  
  def put_state(self, name, newState):
    """ Đặt 1 trạng thái nào đó tới giá trị mới """
    hasChange = (self.state[name] != newState) if name in self.state else True
    if hasChange:
      self.state[name] = newState
      self.set_state(self.state)

  def set_state(self, state):
    """ Áp đặt trạng thái mới xuống thiết bị thật """
    self.state = state
    self.emit_to_real_device(state)
  
  def update_state(self, new_state=None):
    """ Cập nhập trạng thái từ dưới real_device lên virtual_device """
    if new_state is not None:
      self.state = new_state
    self.emit_state_change_event()
  
  def emit_state_change_event(self):
    """ Phát sự kiện có sự thay đổi tới các trạm liên quan, cloud... """
    self.dispatchEvent("statechange", {
      "target": self,
      "state": self.state,
      "reason": self._last_reason
    })
  
  def emit_to_real_device(self, state):
    """ Phát sự kiện có sự thay đổi tới thiết bị thật """
    for real_device in self._real_devices:
      real_device.set_state(state)

  ###########################################

  def dump(self):
    return { "name": self.name, "id": self.id, "roles": self.roles, "state": self.state }

  def stop(self):
    pass