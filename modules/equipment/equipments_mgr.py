

from modules.equipment.virtual_device import VirtualDevice
from modules.equipment.raspi_gpio_device import RaspiGPIODevice
from modules.equipment.esp_device import ESPDevice
from modules.tracker.listenable import Listenable
from modules.tracker.observer import FileModifiedTracker

import platform, json

class EquipmentManager(Listenable):
  def __init__(self, stations_mgr, equipments_file_path='./assets/equipments.json', config_file_path='./configs/stations_cfg.cfg'):
    Listenable.__init__(self, ["equipchange", "statechange"])
    self.equipments = []
    self._real_devices = []
    self.stations_mgr = stations_mgr

    # Nạp trạng thái thiết bị
    self.equipments_file_path = equipments_file_path
    self.equipments = self.load_equipments()

    # Gắn kết thiết bị lên các trạm
    self.equip_stations(self.stations_mgr.stations)

    # Đặt sự kiện để cập nhập lại các liên kết thiết bị khi các trạm thay đổi
    self.stations_mgr.addEventListener("stationschange", self.check_stations_changes)

    # Theo dõi thay đổi trên file
    self.tracking_changes()
  
  @property
  def isRaspi(self):
    return 'Windows' not in platform.system()
  
  def load_equipments(self, equipments_file_path=''):
    """ ## Nạp danh sách thiết bị từ file lên\r\n
    Thông tin mỗi thiết bị: name, id, roles, type, emulate...
    """
    self.equipments_file_path = equipments_file_path if equipments_file_path else self.equipments_file_path
    try:
      with open(self.equipments_file_path, 'r', encoding='utf-8') as fp:
        new_equipments_json = json.load(fp)
        equipments = []
        for equip_info in new_equipments_json:
          new_equip = VirtualDevice(device_info=equip_info)
          new_equip = self.check_equipment(new_equip)
          equipments.append(new_equip)
        print("[System - Equipment Manager] <*> Equipment List loaded: {} equipment(s).".format(len(equipments)))
        return equipments
    except Exception as err:
      print("[System - Equipment Manager] <*> Failed to load Equipment List: {}".format(err))
      return []
  
  def tracking_changes(self):
    self.tracker = FileModifiedTracker()
    self.tracker.track(self.equipments_file_path, self._on_equipments_changes)
  
  def _on_equipments_changes(self, event):
    print("[System - Equipment Manager] <~> Equipments changes!")
    cur_items = self.equipments
    new_items = self.load_equipments(event.src_path)
    try:
      new_counter = 0
      new_list = []
      for new_item in new_items:
        old_items = list(filter(lambda item: item.id == new_item.id, cur_items))
        if old_items:
          for old_item in old_items: old_item.update(new_item)
        else:
          new_list.append(new_item)
          new_counter += 1
      cur_items.extend(new_list)
      if new_counter:
        pass
      print("[System - Equipment Manager] > Updated Equipments! (+{} Equipments)".format(new_counter))
      self.dispatchEvent("equipmentschange", { "target": self, "data": cur_items, "new": new_list })
    except Exception as err:
      print("[System - Equipment Manager] <!> Failed to Update Equipments: {}".format(err))

  def equip_stations(self, stations):
    """ Tìm/Khởi tạo thiết bị ảo gán cho từng trạm.
    Điều này có nghĩa là các trạm sẽ chỉ giữ con trỏ tới các equipment và ta có thể
    chuyển equipment này cho nhiều trạm quản lý. Này dùng trong việc nhiều trạm có chung bơm,
    hay chung giàn phun sương...
    """
    for station in stations:
      old_station_equips = station.equipments
      station.equipments = []
      for equip_info in station.equipments_info:
        for equipment in self.equipments:
          if equip_info["id"] == equipment.id:
            station.attach_equipment(equipment)
      
      # Loại bỏ các VirtualDevice đã không còn trên trạm.
      # (Xảy ra khi có sự thay đổi thông tin trạm và gọi lại về đây)
      for o_equip in old_station_equips:
        for i_equip in station.equipments_info:
          if i_equip["id"] == o_equip.id:
            break
        else:
          self.remove_equipment(o_equip)
    
    # Gỡ bỏ các VirtualDevice của trạm đã bị xóa bỏ
    # (Xảy ra khi có sự thay đổi thông tin trạm và gọi lại về đây)
    for equip in self.equipments:
      for station in stations:
        if station in equip.bound_stations:
          break
      else:
        self.remove_equipment(equip)

    # Gỡ bỏ các Emulated RealDevice không còn nằm trên trạm nào
    for emu_dev in self._real_devices:
      if emu_dev.emulate:
        for station in stations:
          found = False
          for equip in station.equipments:
            # Tìm thông tin giả lập trong thiết bị ở các trạm tương thích với thiết bị này
            # Nếu không tìm thấy tức là thiết bị giả lập này đã bị loại bỏ
            if equip.id == emu_dev.id and 'emulate' in equip.device_info\
              and equip.device_info['emulate'] == emu_dev.device_info['emulate']:
              found = True
              break
          if found:
            break
        else:
          self.detach_real_device(emu_dev)
  
  def check_stations_changes(self, event):
    print("[System - Equipment Manager] <~> Check Stations Changes!")
    self.equip_stations(event["data"])

  def check_equipment(self, new_equip):
    """ Kiểm tra thiết bị đó đã có trong hệ thống chưa, nếu có thì giữ lại, và cập nhập với thông tin của virtual device mới. Đồng thời Thêm thiết bị ảo vào danh sách quản lý của EquipMgr.
    """
    old_eqip = None
    for equip in self.equipments:
      if equip.id == new_equip.id:
        old_eqip = equip
        old_eqip.update(new_equip)
        break
    else:
      self.equipments.append(new_equip)
      new_equip.addEventListener("statechange", self._onStateChange)
    
    choosen_equipment = old_eqip if old_eqip is not None else new_equip
    self.check_new_equipment(choosen_equipment)

    return choosen_equipment
  
  def check_new_equipment(self, equipment):
    delayed_attach_equips = [] # Lưu danh sách GPIO thật/ESP giả lập để chờ gắn kết
    # Với loại thiết bị là GPIO thì có thể attach ngay thiết bị thật
    if isinstance(equipment.id, int):
      for rd in self._real_devices: # Không tạo mới cả GPIO để tránh reset pin
        if rd.id == equipment.id:
          break
      else:
        self.attach_real_device(RaspiGPIODevice(equipment.device_info))
    elif not self.isRaspi:
      if 'ESP' in equipment.id and 'emulate' in equipment.device_info and len(equipment.device_info['emulate']) > 0:
        self.attach_real_device(ESPDevice(equipment.device_info))
    
    # Kiểm tra gắn các device thật vào device ảo mới tạo
    self.rematch_real_device_to_virtual()

  def remove_equipment(self, equip):
    for real_device in self._real_devices:
      if real_device.id == equip.id:
        real_device.detach_virtual_device(equip)
    self.equipments.remove(equip)

  def load_real_device(self, device_info):
    """ Nạp thông tin device thật vào hệ thống. type(device_info): (dict) """
    if "ESP" in device_info['id']:
      real_device = ESPDevice(device_info)
    elif isinstance(device_info['id'], int):
      real_device = RaspiGPIODevice(device_info)
    self.attach_real_device(real_device)
    
    real_device_state = { "state": False }
    for equip in self.equipments:
      if equip.id == real_device.id:
        if equip.state != None:
          real_device_state["state"] = equip.state or real_device_state["state"]
    return real_device_state

  def attach_real_device(self, real_device):
    find_real_devices = [rd for rd in self._real_devices if rd.id == real_device.id]
    # Nếu thiết bị chưa từng tồn tại trong hệ thống thì thêm vào như bình thường
    if not find_real_devices:
      self._real_devices.append(real_device)
      for equip in self.equipments:
        if equip.id == real_device.id:
            equip.attach_real_device(real_device)
    else: # Nếu đã có rồi thì chỉ làm mới lại các thông số cần thiết (socket,...)
      for rd in find_real_devices:
        rd.update(real_device)
  
  def detach_real_device(self, real_device):
    for equip in self.equipments:
      if equip.id == real_device.id:
        equip.detach_real_device(real_device)
    self._real_devices.remove(real_device)
    real_device.detach()
  
  def rematch_real_device_to_virtual(self):
    for real_device in self._real_devices:
      for equip in self.equipments:
        if equip.id == real_device.id:
            equip.attach_real_device(real_device)

  def load_values(self, values, signal):
    """ Nạp dữ liệu cảm biến mới từ StationSync gửi lên. """
    for rd in self._real_devices:
      if rd.are_you(signal):
        rd.values = values
  
  def _onStateChange(self, event):
    self.dispatchEvent("statechange", event)

  ############################################

  def dump(self):
    equipments = []
    for equip in self.equipments:
      equipments.append(equip.dump())
    return equipments

  def stop(self):
    # Stop Virtual devices
    for equip in self.equipments:
      equip.stop()
    
    # Detach Real devices
    for real_device in self._real_devices:
      real_device.detach()