

try: import RPi.GPIO as GPIO
except: import dummy.RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

from modules.equipment.real_device import IRealDevice

import json

class RaspiGPIODevice(IRealDevice):
  def __init__(self, gpio_info, reverse=True):
    super().__init__(gpio_info)
    self.pin = gpio_info['id']
    self.reverse = reverse  # Đảo ngược mức kích, mặc định kích ở mức thấp
    self.isOutput = gpio_info['type'] == "output"
    self._state = False

    GPIO.setup(self.pin, GPIO.OUT if self.isOutput else GPIO.IN)
    GPIO.output(self.pin, GPIO.LOW^self.reverse)
    
    if not self.isOutput:
      GPIO.add_event_detect(self.pin, GPIO.BOTH, callback=self.on_change)

  @property
  def connected(self):
    return True

  def update(self, real_device):
    pass
  
  def are_you(self, signal):
    return False

  ############################################
  
  @property
  def state(self):
    return self._state

  @state.setter
  def state(self, state):
    self._state = state
  
  def get_state_json(self):
    return json.dumps(self.state, ensure_ascii=False)

  def set_state(self, state):
    """ Đặt trạng thái từ trên xuống """
    self.state = state
    GPIO.output(self.pin, list(state.values())[0]^self.reverse)

  def update_state(self, szState):
    """ Cập nhập trạng thái từ thiết bị thật gửi lên """
    try:
      # self.state = json.loads(szState.decode('utf-8') if isinstance(szState, bytes) else szState, encoding="utf-8")
      self.state = szState
      self.virtual_device.update_state(self.state) # Cập nhập trạng thái lên trên
    except Exception as err:
      print("[Device - GPIO] > Parsing error: {}".format(err))

  def request_update_state(self):
    """ Nhận được yêu cầu làm mới lại dữ liệu cảm biến (từ server gửi xuống trạm). """
    return self.update_state(GPIO.input(self.pin))
  
  def on_change(self, pin, state):
    pass
  
  def enable_auto_update(self):
    self.update_state(GPIO.input(self.pin))
  
  ############################################
  
  def stop(self):
    pass