


class Layer:
  def __init__(self, num_noron, init_val):
    self.num_noron = num_noron
    self.init_val = init_val
    self.load()
  
  def load(self):
    self.norons = []
    for i in range(self.num_noron):
      self.norons.append(self.init_val)
