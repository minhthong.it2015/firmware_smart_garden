

import socket
from time import sleep, time

last = 0
life = 0
first = True
while True:
  try:
    s = socket.socket()
    s.connect(("192.168.244.1", 80))
    s.send(b"GetRaspi\n")
    ip = s.recv(100)
    now = time()
    if len(ip) > 1:
      first = True
      if now - last > 6: # lần đầu tiên kết nối lại sau khi lỗi
        life = time()
      print(ip, now - last if now - last > 6 else '')
      last = time()
    sleep(5)
  except BaseException as err:
    if isinstance(err, KeyboardInterrupt):
      break
    if first:
      first = False
      print("life time: ", time() - life)
    sleep(2)