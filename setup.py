#!/usr/bin/python3
# -*- coding=utf-8 -*-

# git clone https://minhthong.it2015%40gmail.com:Inthebetterworld96@gitlab.com/minhthong.it2015/firmware_smart_garden.git

from modules.helper.setup_helper import FirmwareService, install_dependences, enable_excutable_files

import argparse

##########################################################
###                       Services                     ###
##########################################################

DEPENDENCES = {
  "library": ["websockets", "websocket-client", "wsaccel", "numpy"]
}


##########################################################
###                       Services                     ###
##########################################################

FIRMWARE_SERVICES = [
  {
    "name": "InSys Core Firmware",
    "exec_file": "startup",
    "description": "INSYS SMART GARDEN - CORE SERVICE",
    "enable": True
  }, {
    "name": "InSys Updater",
    "exec_file": "update",
    "description": "INSYS SMART GARDEN - UPDATE SERVICE",
    "enable": False
  }, {
    "name": "InSys Tracker",
    "exec_file": "camera",
    "description": "INSYS SMART GARDEN - CAMERA TRACKING SERVICE",
    "enable": False
  }
]

SERVICES = [FirmwareService(service_info['name'], service_info['exec_file'], service_info['description'], service_info['enable']) for service_info in FIRMWARE_SERVICES]

def install_firmware_services():
  for service in SERVICES:
    service.install()

def restart_firmware_services():
  for service in SERVICES:
    service.restart()

def stop_firmware_services():
  for service in SERVICES:
    service.stop()

EXECUTABLE_FILES = ['setup.py', 'update.py', 'main.py', 'startup']

def default_setup():
  install_dependences(DEPENDENCES)
  install_firmware_services()
  enable_excutable_files(EXECUTABLE_FILES)

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='InSys Firmware Setup')
  parser.add_argument('-x', action='store_true', help='Enable excutable files')
  parser.add_argument('-s', action='store_true', help='Stop All services')
  parser.add_argument('-sv', action='store_true', help='Install services')
  parser.add_argument('-rs', action='store_true', help='Restart services')

  args = parser.parse_args()

  default = True
  if args.sv:
    print('[Setup] > Install services')
    default = False
    install_firmware_services()

  if args.x:
    default = False
    print('[Setup] > Enable excutable files')
    enable_excutable_files(EXECUTABLE_FILES)
  
  if args.s:
    default = False
    print('[Setup] > Stop all services')
    stop_firmware_services()

  if args.rs:
    print('[Setup] > Restart services')
    default = False
    restart_firmware_services()

  if default:
    default_setup()