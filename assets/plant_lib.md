


# Hướng dẫn viết trong file plant lib
-----------------------------------

1. Mỗi cây có các thuộc tính 
  - `plant_name`: Tên loại cây
  - `plant_type`: ID loại cây
  - `growth_stages`: Mảng lưu các giai đoạn phát triển của cây
    + `stage_order`: Thứ tự của giai đoạn để sắp xếp tăng dần.
    + `stage_name`: Tên giai đoạn.
    + `start`: Ngày bắt đầu giai đoạn (Tính từ 1).
    + `end`: Ngày kết thúc giai đoạn.
    + `living_environment`: Thông tin điều kiện môi trường tối ưu.
      + `usefor`: Sử dụng cho mô hình sinh trưởng nào (hydroponics, aeroponics, aquaponics, geoponics).
      + `environments`: Mảng lưu các điều kiện môi trường. Mỗi điều kiện quy định như sau:
        + `name`: Tên điều kiện. Được liệt kê trong ENV_TYPE_MAPPING (modules.plant.environment_factors). Hiện tại bao gồm [`water`, `temperature`, `humidity`, `light`, `rotate`, `ppm`, `pH`].
        + Mỗi điều kiện sẽ có thể cài đặt thuộc tính sau:
          + [Water]:
            + `water`: Mảng các thời điểm bơm tưới (xem mục 2 và 3)
            + `actions_on`: Mảng lưu danh sách các hành động khi tới giờ tưới nước
            + `actions_off`: Mảng lưu danh sách các hành động khi hết giờ tưới nước
          + [temperature]:
            + `temperature`: [<min_temp>, <max_temp>, <offset>]
              + <min_temp>, <max_temp>, <offset>: Khoảng giá trị
            + `subthreshold`: Hành động khi thấp dưới ngưỡng (xem tại mục 4)
            + `superthreshold`: Hành động khi vượt quá ngưỡng (xem tại mục 4)
            + `inrange`: Hành động khi ở trong ngưỡng (xem tại mục 4)
          + [humidity]: Chưa thiết lập.
          + [light]:
            + `light`: Đối tượng lưu thông tin quản lý ánh sáng
              + `lux_range`: [<min_light>, <max_light>, <offset>]
              + `time_range`: Mảng các thời điểm chiếu sáng (xem mục 2 và 3)
          + [rotate]:
            + `rotate`: Mảng các thời điểm xoay trụ (xem mục 2 và 3)
          + [ppm]: Chưa thiết lập.
          + [pH]: Chưa thiết lập.

2. Cấu trúc thời điểm bơm tưới:
  Có 3 cách định giờ chính:
  ---
  - [1] Bắt đầu tại một thời điểm xác định và kéo dài trong 1 khoảng thời gian:
    VD 1: Bắt đầu lúc 6h30 và 18h hàng ngày, mỗi lần 30 phút
    { `begin`: [`6:30`, `18:00`], `duration`: `30` } (*) Dạng rõ ràng
    { `begin`: [`6:30-30`, `18:00-30`] } (*) Dạng tích hợp duration
    { `begin`: [`6:30-15`, `18:00-30`], `duration`: `30` } (*) duration ở trong `begin` có ưu tiên cao hơn
    VD 2: Bắt đầu lúc 6h30 và 18h mỗi 3 ngày, mỗi lần 30 phút
    { `begin`: [`6:30`, `18:00`], `duration`: `30`, `cycle`: `3:0:0:0` } (*) Dạng rõ ràng
    { `begin`: [`6:30-30`, `18:00-30`], `cycle`: `3:0:0:0` } (*) Dạng tích hợp duration
    { `begin`: [`6:30~3:0:0:0`, `18:00~3:0:0:0`], `duration`: `30` } (*) Dạng tích hợp cycle
    { `begin`: [`6:30-30~3:0:0:0`, `18:00-30~3:0:0:0`] } (*) Dạng tích hợp cả duration và cycle
    { `begin`: [`6:30-30~3:0:0:0`, `18:00-30~3:0:0:0`], `duration`: `30`, `cycle`: `3:0:0:0` } (*) duration và cycle ở trong có ưu tiên cao hơn

  - [2] Bắt đầu và kết thúc tại các thời điểm xác định (Không có duration)
    VD 1: Thực hiện vào lúc từ 6h30 tới 7h và 18h tới 18h30 hàng ngày
    { `time_range`: [`6:30-7:00`, `18:00-18:30`] }
    VD 2: Thực hiện vào lúc từ 6h30 tới 7h mỗi và 18h tới 18h30 mỗi 3 ngày
    { `time_range`: [`6:30-7:00`, `18:00-18:30`], `cycle`: `3:0:0:0` }
    { `time_range`: [`6:30-7:00~3:0:0:0`, `18:00-18:30~3:0:0:0`] } (*) Dạng tích hợp
    VD 3: Thực hiện vào lúc từ 6h30 tới 7h mỗi 2 ngày và 18h tới 18h30 mỗi 3 ngày
    { `time_range`: [`6:30-7:00~2:0:0:0`, `18:00-18:30~3:0:0:0`] }
    { `time_range`: [`6:30-7:00~2:0:0:0`, `18:00-18:30~3:0:0:0`], `cycle`: `3:0:0:0` } (*) cycle ở trong `time_range` có ưu tiên cao hơn

  - [3] Thực hiện hành động lặp lại theo chu kỳ ngắn
    VD 1: Mỗi 1h thực hiện 15 phút và mỗi 6h thực hiện 15 phút
    { `every`: [`1:00`, `6:00`], `duration`: `15` }
    VD 1: Mỗi 1h thực hiện 15 phút và mỗi 6h thực hiện 30 phút
    { `every`: [`1:00-15`, `6:00-30`] } (*) Dạng tích hợp duration
    VD 2: Chu kỳ như VD 1 nhưng được giới hạn thời gian trong 1 khoảng từ 18h tới 6h sáng ngày hôm sau
    { `every`: [`1:00`, `6:00`], `duration`: `15`, `from`: `18:00`, `to`: `6:00` } (*) Dạng rõ ràng
    { `every`: [`1:00-15`, `6:00-30`], `from`: `18:00`, `to`: `6:00` } (*) Dạng tích hợp duration
    { `every`: [`1:00|18:00-6:00`, `6:00|18:00-6:00`], `duration`: `15` } (*) Dạng tích hợp limit
    { `every`: [`1:00-15|18:00-6:00`, `6:00-30|18:00-6:00`] } (*) Dạng tích hợp duration và limit
    VD 3: Chu kỳ như VD 1 nhưng được giới hạn thời gian trong 1 khoảng từ 18h tới 6h sáng ngày hôm sau mỗi 3 ngày. Cycle này là dành cho from-to, tức là mỗi 3 ngày thì mới thực hiện hành động từ 18h tới 6h sáng theo chu kỳ.
    { `every`: [`1:00`, `6:00`], `duration`: `15`, `from`: `18:00`, `to`: `6:00`, `cycle`: `1:0:0:0` } (*) Dạng rõ ràng
    { `every`: [`1:00-15`, `6:00-15`], `from`: `18:00`, `to`: `6:00`, `cycle`: `1:0:0:0` } (*) Dạng tích hợp duration
    { `every`: [`1:00-15|18:00-6:00`, `6:00-15|18:00-6:00`], `cycle`: `1:0:0:0` } (*) Dạng tích hợp duration, limit
    { `every`: [`1:00-15|18:00-6:00~1:0:0:0`, `6:00-15|18:00-6:00~1:0:0:0`] } (*) Dạng tích hợp duration, limit và cycle

  - [4] Tích hợp dạng [1] và [2]
    { `begin`: [`6:30`, `18:00`], `duration`: `30`, `time_range`: [`6:30-7:00`, `18:00-18:30`] }

3. Cú pháp thời gian: ( định dạng: a[:b[:c[:d]]] )
  - 1 số: (minutes)
  - 2 số: (hours, minutes)
  - 3 số: (hours, minutes, seconds)
  - 4 số: (days, hours, minutes, seconds)

4. Cú pháp hành động: {`action`: <Mảng các hành động> [,`reason`: <Lý do thực hiện hành động>]}
  - Bắt đầu một hành động:
    + VD: {`action`: [`bump-on`, `light-on`]}
  - Kết thúc một hành động:
    + VD: {`action`: [`bump-off`, `light-off]}
  - Thực hiện hành động trong một khoảng thời gian, hết thời gian sẽ chuyển về off. Thời gian cú pháp như mục 3.
    + VD: {`action`: [`bump-on-15`, `light-on-1:0:0]}
  - Canh ngắt hành động sau một khoảng thời gian:
    + VD: {`action`: [`bump-off-15`, `light-off-1:0:0]}
  - Thực hiện hành động với lý do xác định. Chỉ khi không còn lý do nào mới ngắt hành động:
    + VD: {`action`: [`misting-on-15`], `reason`: `high_temp`}
    + VD: {`action`: [`misting-off-5`], `reason`: [`high_temp`, `low_temp`]}
  * Ghi chú thêm một số hành động khả dụng hiện tại (8/7/2018):
    - `misting`: Bơm phun sương
    - `bump`: Bơm nước/dinh dưỡng
    - `light`: Bật đèn nông nghiệp