
# Mô tả cấu trúc dữ liệu trong `equipments.json`
--------------------------------------------

1. Hướng dẫn:
  - Các thiết bị trong `equipments.json` được tự động thêm từ thiết bị thật khi kết nối.
  - Các thông tin liên quan được lưu trong thiết bị thật.
  - Chỉ tạo thủ công với thiết bị GPIO.

2. Thuộc tính cơ bản thiết bị:
  - `name`: Tên người dùng tự đặt cho thiết bị.
  - `id`: Định danh của thiết bị trong hệ thống. Hoạt động như là cầu nối giữa thiết bị thật và thiết bị ảo.
    + Ngoài ra thiết bị thật sẽ có thêm id khác để phân biệt.
    + Nếu `id` là số nó sẽ được thiết đặt gắn kết ngay với GPIO của raspi. Nếu là chữ sẽ chờ thiết bị ngoài gắn kết tới.
  - `roles`: [<sensors>, <controls>]
    Liệt kê các chức năng thiết bị. VD: { "roles": ["light_control", "heater"] }, { "roles": ["sensors"] }
  - `state`: Lưu lại trạng thái hiện tại của thiết bị.
  - `emulate` (opt): [<sensors>, <controls>]
    Mảng các tính năng muốn giả lập. VD: { "emulate": ["sensors"] }
  - `type` (opt): Dùng với GPIO device, quy định pin GPIO dùng phát tín hiệu hay nhận tín hiệu.
  
  * <sensors>: [`temperature`, `humidity`, `ppm`, `light`, `pH`]
  * <controls>: [`misting`, `fan`, `ppm_control`, `light_control`, `pH_control`, `pump`, `heater`]
  * Ngoại trừ <sensors> sẽ không biết có những cảm biến gì, còn lại các loại <controls> đều khởi động với trạng thái [false] theo mặc định.

VD:
> { "name": "Bơm 01", "id": 13, "type": "output", "roles": ["pump", "nutri"] },