

# Hướng dẫn viết file stations.json
-----------------------------------

File stations.json lưu một mảng thông tin các trạm

1. Cấu trúc thông tin mỗi trạm:
  - `name`: Tên trạm
  - `id`: Định danh trạm
  - `type`: Loại trạm (Loại này là tên bản thiết kế)
  - `env_type`: Mảng kiểu mô hình sinh trưởng được thể hiện trên trạm. Này có thể có các giá trị sau:
    + `hydroponics` : Trạm Thủy canh
    + `aeroponics`  : Trạm Khí canh
    + `aquaponics`  : Trạm Thủy canh có bể cá
    + `geoponics`   : Trạm Địa canh
  - `equipments`: Mảng lưu danh sách thiết bị đính kèm lên trạm
    + Dạng thiết bị Raspi GPIO: Được điều khiển trực tiếp từ raspi.
      + `id`: Định danh thiết bị
      + `type`: `control` hay `sensors`. Dùng để biết chân GPIO để nhận tín hiệu hay điều khiển.
      + `roles`: Mảng các vai trò thiết bị. (`bump`, `led`, `nutri`, `misting`...)
    + Dạng thiết bị kết nối qua LAN: Ở đây hiện tại dùng ESP wifi.
      + `id`: Định danh thiết bị
      + `roles`: Mảng các vai trò thiết bị. (`bump`, `led`, `nutri`, `misting`...)
      + `emulate`: Mảng lưu các dạng giả lập muốn có trên thiết bị. Này sẽ tạo thiết bị thật trong chương trình để giả lập chứ không phải dùng nguyên VirtualDevice.
        + `sensors`: Giả lập cảm biến. Sẽ tạo mẫu tất cả thông số môi trường.
        + `control`: Giả lập thiết bị điều khiển. Nhận lệnh và emit event trên nó.
