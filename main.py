#!/usr/bin/python3
# coding=utf-8
"""
"""

from modules.controllers.controller_type1.controller_t1 import ControllerType1

if __name__ == "__main__":
  print("\r\n-----------------------------------------------------------------------\r\n")
  controller = ControllerType1()
  controller.run()