
import asyncio
import websockets
import threading
from time import sleep


class WSServer:
  def __init__(self):
    self.clients = []

  def recv_message(self, socket, result):
    socket.waiting = False
    socket.data = result._result
    if len(socket.data) > 0:
      asyncio.ensure_future(socket.send(socket.data))

  async def request_handle(self, socket, path):
    print("[WebSocket] > Connection from {} (path: {}).".format(socket.remote_address, path))
    try:
      if not [x for x in self.clients if x.remote_address == socket.remote_address]:
        self.clients.append(socket)
        
      socket.waiting = False
      socket.data = ''
      while True:
        if not socket.waiting:
          socket.waiting = True
          future = asyncio.ensure_future(socket.recv())
          future.add_done_callback(lambda rs: self.recv_message(socket,rs))
        else:
          await asyncio.sleep(.01)
    except Exception as err:
      if socket in self.clients:
        self.clients.remove(socket)
      print("[Websocket] > Client disconnected: {} ({}).".format(socket.remote_address, err))
  
  def start(self):
    self.server = websockets.serve(self.request_handle, "0.0.0.0", 1234)
    self.server = asyncio.get_event_loop().run_until_complete(self.server)

    self.server.close()
    asyncio.get_event_loop().run_until_complete(self.server.wait_closed())

class AnotherThread:
  def __init__(self, server, loop):
    self.server = server
    t = threading.Thread(target=self.loop, args=(loop,))
    t.start()
  
  def loop(self, loop):
    asyncio.set_event_loop(loop)
    while True:
      sleep(3)
      if len(self.server.clients) > 0:
        print("Looping...")
        for client in self.server.clients:
          future = asyncio.ensure_future(client.send("hello"))
          future.add_done_callback(lambda rs: print("result:",rs))

loop = asyncio.get_event_loop()

server = WSServer()
test = AnotherThread(server, loop)
server.start()

try:
  loop.run_forever()
except KeyboardInterrupt:
  print("keyboard interrupt")
  pass
finally:
  server.server.close()
  asyncio.get_event_loop().run_until_complete(server.server.wait_closed())
  loop.stop()